package it.unipd.dei.imd.rum.anova.utilities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import it.unipd.dei.ims.rum.utilities.UsefulConstants;

/** Script class to prepare an csv file
 * with the data of the times
 * for each of the 50 queries for each of 
 * the 6 algorithms. 
 * 
 * */
public class PrepareTimeDataAnova {

	/** The files with our data, one for each algorithm.
	 * The supposed order is the following:
	 * Blanco, Yosi, TSA+BM25, TSA+Blanco,
	 * TSA+Ysi, TSA+Pruning
	 * */
	private String[] filesToParse;

	/** where to write the csv file
	 * */
	private String outputFile;

	public PrepareTimeDataAnova() {

	}

	/** Creates a matrix using the data read from scripts.
	 * The columns of the matrix represent the queries,
	 * the rows represent the algorithms.
	 * <p>
	 * Originally though to deal with my time scripts. 
	 * 
	 * @param columns the number of queries and thus columns to have in the csv file
	 * */
	public void createMatrixFileFor1WayAnova(int columns) {
		//create our matrix
		List<List<Double>> matrix = new ArrayList<List<Double>>();

		//instantiate the memory for the matrix
		for(int i = 0; i< filesToParse.length; ++i) {
			matrix.add(new ArrayList<Double>(Collections.nCopies(columns, 0.0)));
		}


		//for each one of the time files
		for(int i = 0; i < filesToParse.length; ++i) {
			String line = "";
			//get the file and open a reader stream
			String file = filesToParse[i];
			Path inputPath = Paths.get(file);
			try(BufferedReader reader = Files.newBufferedReader(inputPath)) {
				//read these lines
				while((line = reader.readLine()) != null) {
					if(line.contains("complexively  required:")) {
						/*this covers 
						 * TSA+BM25
						 * TSA+Blanco, 
						 * TSA+Yosi
						 * TSA+PRUNING
						 * */
						this.dealWithOneQuery(line, matrix, i);
					} else if(line.contains("the total time required to compute the query")) {
						/*this covers
						 * YOSI*/
						this.dealWithBlancoAndYosiQuery(line, matrix, i);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		//now print the matrix
		this.printTheMatrix(matrix);
	}
	
	private void printTheMatrix(List<List<Double>> matrix) {
		Path outputPath = Paths.get(this.outputFile);
		try(BufferedWriter writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING); ) {
			for(int i = 0; i < matrix.size(); ++i) {//for each row of the matrix
				int columnCounter = 0;
				List<Double> row = matrix.get(i);
				for(Double value : row) {
					writer.write(""+value);
					if(columnCounter < row.size() - 1) {
						writer.write(", ");
						columnCounter++;
					}
				}
				writer.newLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** Given the line with the information, it inserts the information in the
	 * right spot
	 * 
	 * @param i the row to tackle, that is, the index of
	 * the algorithm we are dealing with
	 * */
	private void dealWithOneQuery(String line, List<List<Double>> matrix, int i) {
		//take the number of the query
		String[] parts = line.split(" ");
		String queryId = parts[2];
		int qID = Integer.parseInt(queryId);

		//now take the value of time
		parts = line.split(": ");
		String time = parts[1];
		//take the absolute value of time
		String absTime = time.split(" ")[0];
		String unitOfMeasure = time.split(" ")[1];
		Double timeInSeconds = Double.parseDouble(absTime); 

		if(unitOfMeasure.trim().equals("s")) {
			//it's ok
		} else if(unitOfMeasure.trim().equals("min")) {
			//convert in seconds
			timeInSeconds = timeInSeconds*60;
		}

		//get the row
		List<Double> row = matrix.get(i); 
		Double value = row.get(qID - 1);//the qury IDs are ranks, we want indexes
		if(value == 0.0) {
			row.set(qID - 1, timeInSeconds);
		} else {
			//...DEBUG
		}
	}

	public void dealWithBlancoAndYosiQuery(String line, List<List<Double>> matrix, int i) {
		//take the number of the query
		String[] parts = line.split(" ");
		String queryId = parts[8];
		int qID = Integer.parseInt(queryId);

		//now take the value of time
		parts = line.split(": ");
		String time = parts[1];
		//take the absolute value of time
		String absTime = time.split(" ")[0];
		String unitOfMeasure = time.split(" ")[1];
		Double timeInSeconds = Double.parseDouble(absTime); 

		if(unitOfMeasure.trim().equals("s")) {
			//it's ok
		} else if(unitOfMeasure.trim().equals("min")) {
			//convert in seconds
			timeInSeconds = timeInSeconds*60;
		}

		//get the row
		List<Double> row = matrix.get(i); 
		Double value = row.get(qID - 1);
		if(value == 0.0) {
			row.set(qID - 1, timeInSeconds);
		} else {
			//...DEBUG
		}
	}

	public static void main(String[] args) {
		//insert here the file to parse 
		//to obtain the time data
		
		// ****** rLinkedMDB ******
		/*
		 * String[] filesToParse = {
		 
				"/Users/dennisdosso/Documents/RDF_DATASETS/rLinkedMDB/algorithms/BLANCO/queries/times.txt",
				"/Users/dennisdosso/Documents/RDF_DATASETS/rLinkedMDB/algorithms/YOSI/queries/times.txt",
				"/Users/dennisdosso/Documents/RDF_DATASETS/rLinkedMDB/algorithms/TSA/queries_filter_100/times/times_IR.txt",
				"/Users/dennisdosso/Documents/RDF_DATASETS/rLinkedMDB/algorithms/TSA/queries_filter_100/times/times_BLANCO.txt",
				"/Users/dennisdosso/Documents/RDF_DATASETS/rLinkedMDB/algorithms/TSA/queries_filter_100/times/times_YOSI.txt",
				"/Users/dennisdosso/Documents/RDF_DATASETS/rLinkedMDB/algorithms/TSA/queries_filter_100/times/times_PRUNING.txt"
		};
		//path where to write the csv file
		String outputPath = "/Users/dennisdosso/Desktop/ANOVA tests/2_ways/linked_mdb_times.csv";
		
		*/
		
		
		// ********** rIMDB **********
//		String[] filesToParse = {
//				"/Users/dennisdosso/Documents/RDF_DATASETS/rIMDB/algorithms/BLANCO/queries/times.txt",
//				"/Users/dennisdosso/Documents/RDF_DATASETS/rIMDB/algorithms/YOSI/queries/times.txt",
//				"/Users/dennisdosso/Documents/RDF_DATASETS/rIMDB/algorithms/TSA/queries/ir_times.txt",
//				"/Users/dennisdosso/Documents/RDF_DATASETS/rIMDB/algorithms/TSA/queries/blanco_times.txt",
//				"/Users/dennisdosso/Documents/RDF_DATASETS/rIMDB/algorithms/TSA/queries/yosi_times.txt",
//				"/Users/dennisdosso/Documents/RDF_DATASETS/rIMDB/algorithms/TSA/queries/times_PRUNING.txt"
//		};
//		//path where to write the csv file
//		String outputPath = "/Users/dennisdosso/Desktop/ANOVA tests/2_ways/imdb_mdb_times.csv";
		
		// ********** LinkedMDB ********** //
		String[] filesToParse = {
				"/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/algorithms/TSA/queries/times_TSA+IR.txt",
				"/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/algorithms/TSA/queries/times_TSA+VDP.txt"
		};
		
		String outputPath = "/Users/dennisdosso/Desktop/imdb_times.csv";
		
		PrepareTimeDataAnova execution = new PrepareTimeDataAnova();
		execution.setFilesToParse(filesToParse);
		execution.setOutputFile(outputPath);
		
		execution.createMatrixFileFor1WayAnova(50);
		
		System.out.println("all done");
	}

	public String[] getFilesToParse() {
		return filesToParse;
	}

	public void setFilesToParse(String[] filesToParse) {
		this.filesToParse = filesToParse;
	}

	public String getOutputFile() {
		return outputFile;
	}

	public void setOutputFile(String outputFile) {
		this.outputFile = outputFile;
	}

}
