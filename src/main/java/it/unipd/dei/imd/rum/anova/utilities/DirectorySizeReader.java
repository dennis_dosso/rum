package it.unipd.dei.imd.rum.anova.utilities;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.FileUtils;

import it.unipd.dei.ims.rum.utilities.UsefulConstants;

/** Some methods to compute the sizes of directories
 * 
 * */
public class DirectorySizeReader {

	/** Path of the directory to read
	 * */
	private String directoryPath;

	/** Paths with the directories to be measured */
	private String[] directoriesToMeasure;

	public DirectorySizeReader() {

	}

	/** test method, don't bother
	 * */
	public void readDirectorySize(String dirPath) {
		long size = this.readDirectorySizeInBytes(dirPath);
		String humanReadableSize = this.fromBytesToHumanReadableSize(size);
		System.out.println(humanReadableSize);
	}

	/** Reads the size in bytes of a directory, with all 
	 * the content. Returns the row number in bytes.
	 * Then maybe you should use {@link fromBytesToHumanReadableSize}
	 * to have a useful string.
	 *  */
	public long readDirectorySizeInBytes(String dirPath) {
		File folder = new File(dirPath);
		//get the sie of the folder in bytes
		long size = FileUtils.sizeOfDirectory(folder);
		return size;
	}

	/** Converts a long into a corresponding human readable 
	 * size, for example in Bytes or KB of MB etc.
	 * We go up until TB.
	 * */
	public String fromBytesToHumanReadableSize(long size) {
		if(size == 0)
			return "0 MB";
		
		String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
		int unitIndex = (int) (Math.log10(size) / 3);
		double unitValue = 1 << (unitIndex * 10);

		String readableSize = new DecimalFormat("#,##0.#")
				.format(size / unitValue) + " "
				+ units[unitIndex];

		return readableSize;
	}

	/** Given a starting directory, this method
	 * computes the average occupied space by the subdirectories
	 * of the first level of the main directory.
	 * 
	 * */
	public String getAverageMemoryInsideMainDirectory(String mainDirectory, String addOn) {
		File mainFolder = new File(mainDirectory);
		//list the quer
		File[] files = mainFolder.listFiles();
		long size = 0;
		int counter = 0;
		//sum all the sizes
		for(File folder : files) {
			if(!folder.isDirectory())
				continue;

			String dirPath = folder.getAbsolutePath();
			if(addOn!=null) {
				dirPath += "/" + addOn;
			}

			size += this.readDirectorySizeInBytes(dirPath);
			counter++;
			if(counter % 10 == 0)
				System.out.println("read " + counter + " folders" );
		}
		//compute the mean
		size = (long) size / counter;

		String humanReadableSize = this.fromBytesToHumanReadableSize(size);

		return humanReadableSize;
	}

	/**  This methods reads the paths passed in the parameter array
	 * and computes the size of all the directories inside them. 
	 * Then creates a matrix with these sizes in the row. 
	 * One row for each element of the array
	 * 
	 * @param directories an array of strings with the paths of the
	 * directories containing the query folders whose sizes have 
	 * to be measured.
	 * @param addOn an array with strings representing possible add ons to add to the paths
	 * (it is necessary because I put all the sub-algorithms based
	 * on TSA on the same directory. Fool)
	 * */
	public void createMatrixOFSizes(String[] directories, String[] addOn, String outputFile) {
		//create the matrix
		List<List<Long>> matrix = new ArrayList<List<Long>>();

		//instantiate the memory for the matrix
		for(int i = 0; i< directories.length; ++i) {
			matrix.add(new ArrayList<Long>((Collections.nCopies(50, (long) 0)))) ;//a lot of parenthesis 
		}

		for(int i = 0; i < directories.length; ++i) {
			//get the current row
			List<Long> row = matrix.get(i);
			
			//get the path of the query directory
			String startPath = directories[i];
			
			for(int j = 1; j <= 50; ++j) {
				//operate on the 50 queries

				//get the path of the query folder
				String path = startPath + "/" + (j);
				String add = addOn[i];
				if(add!=null)
					path = path + "/" + add;
				
				File f = new File(path);
				if(!f.isDirectory())
					continue;
				if(f.getName().equals("times"))
					continue;

				//take the size in bytes
				long size = this.readDirectorySizeInBytes(path);
				
				//add the value in the matrix
				row.set(j-1, size);

			}
		}
		//here we have completed the matrix
		
		//we need to adjust two or three little things
		//this.correctTheMatrix(matrix);
		this.correctTheMatrixForBigMatrices(matrix);
		
		//now print the matrix in a csv file
		this.printTheMatrix(matrix, outputFile);
	}
	
	/** Given the nature of the algorithms, I 
	 * use this method to correct the values we read.
	 * <p>
	 * In this case, add the row of index 2 to the 
	 * rows of index 3, 4 and 5, because the methods 3, 4 and 5
	 * use the data created in method 2.
	 * */
	private void correctTheMatrix(List<List<Long>> matrix) {
		List<Long> irRow = matrix.get(2);
		for(int i = 3; i < matrix.size(); ++ i) {
			List<Long> addendum = matrix.get(i);
			for(int j = 0; j < irRow.size(); ++ j) {
				long val = addendum.get(j);
				val += irRow.get(j);
				addendum.set(j, val);
			}
		}
	}
	
	private void correctTheMatrixForBigMatrices(List<List<Long>> matrix) {
		List<Long> irRow = matrix.get(0);
		for(int i = 1; i < matrix.size(); ++ i) {
			List<Long> addendum = matrix.get(i);
			for(int j = 0; j < irRow.size(); ++ j) {
				long val = addendum.get(j);
				val += irRow.get(j);
				addendum.set(j, val);
			}
		}
	}
	
	private void printTheMatrix(List<List<Long>> matrix, String outputPath) {
		Path outputP = Paths.get(outputPath);
		try(BufferedWriter writer = Files.newBufferedWriter(outputP, UsefulConstants.CHARSET_ENCODING); ) {
			for(int i = 0; i < matrix.size(); ++i) {//for each row of the matrix
				int columnCounter = 0;
				List<Long> row = matrix.get(i);
				for(Long value : row) {
					writer.write(""+value);
					if(columnCounter < row.size() - 1) {
						writer.write(", ");
						columnCounter++;
					}
				}
				writer.newLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/** Taken as imput a parent directory, produces a csv file with all the sizes of the
	 * sub directories of the firs level in the tree.
	 * 
	 * @param mainDir main directory
	 * @param outputFile the file to output where I write things
	 * */
	public void createCSVOfSizes(String mainDir, String addOn, String outputFile) {
		File mDir = new File(mainDir);
		//get all the subdirectories
		File[] files = mDir.listFiles();
		for(File f : files) {
			
			if(!f.isDirectory())
				continue;
			
			//take the size in bytes
			long size = this.readDirectorySizeInBytes(f.getAbsolutePath() + "/" + addOn);
			System.out.println(fromBytesToHumanReadableSize(size));
		}
	}


	/** Test main */
	public static void main(String[] args) {
		
		// ********** AVERAGE SPACE OF A GROUP OF DIRECTORIES ********** //
		/*
		String dirPath = "/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/algorithms/TSA/clusters";
		DirectorySizeReader execution = new DirectorySizeReader ();

		//		execution.readDirectorySize(dirPath);

		//get the average sizes of a directory
		String averageSize = execution.getAverageMemoryInsideMainDirectory(dirPath, null);
		System.out.print(averageSize);
		*/
		
		
		//****** CONSTRUCTION OF A MATRIX OF SPACES ********** //
		
//		//for LinkedMDB
//		String[] folders = {"/Users/dennisdosso/Documents/RDF_DATASETS/rIMDB/algorithms/BLANCO/queries",
//				"/Users/dennisdosso/Documents/RDF_DATASETS/rIMDB/algorithms/YOSI/queries",
//				"/Users/dennisdosso/Documents/RDF_DATASETS/rIMDB/algorithms/TSA/queries",//IR
//				"/Users/dennisdosso/Documents/RDF_DATASETS/rIMDB/algorithms/TSA/queries",//BLANCO
//				"/Users/dennisdosso/Documents/RDF_DATASETS/rIMDB/algorithms/TSA/queries",//YOSI
//				"/Users/dennisdosso/Documents/RDF_DATASETS/rIMDB/algorithms/TSA/queries"};//PRUNING
		
		String[] folders = {"/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/algorithms/TSA/reduced_queries", 
				"/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/algorithms/TSA/reduced_queries"};
		
//		String[] addOn = {null, null, 
//				"IR", "BLANCO", "YOSI", "PRUNING"};
		
		String[] addOn = {"IR", "PRUNING"};
		
		String outputFile = "/Users/dennisdosso/Desktop/time_matrix_imdb.csv";
		
		DirectorySizeReader execution = new DirectorySizeReader();
		
		
		//execution.createMatrixOFSizes(folders, addOn, outputFile);
		
		String mainDir = "/Users/dennisdosso/Documents/RDF_DATASETS/LUBM10M/systems/TSA/queries";
		String addON = "";
		execution.createCSVOfSizes(mainDir, addON, outputFile);
		
		
	}


}
