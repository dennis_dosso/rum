package it.unipd.dei.imd.rum.anova.utilities;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import it.unipd.dei.ims.rum.utilities.UsefulConstants;

/** This class is de facto a script to create the file
 * to perform the ANOVA execution later in matlab.
 * */
public class SetupAnovaFile {

	/** Arrays of string with the levels used in the ANOVA
	 * */
	private String[] level1, level2, level3;


	private String outputFile;

	public SetupAnovaFile(String out) {
		this.outputFile = out;
	}


	public void printSetupFileFor3WayAnova(String name1, int rep1, 
			String name2, int rep2,
			String name3, int rep3) {
		//the total length of our rows
		int everythingIsNotEnough = rep1*rep2*rep3;

		int counter = 0;

		List<String> list1 = new ArrayList<String>( everythingIsNotEnough );
		List<String> list2 = new ArrayList<String>( everythingIsNotEnough );
		List<String> list3 = new ArrayList<String>( everythingIsNotEnough );

		//we prepare the lists of string in a way they can be written down
		for(int i = 1; i <= rep1; ++i) {
			for(int j = 1; j <= rep2; ++j) {
				for(int k = 0; k < rep3; ++k ) {
					list1.add(counter, name1+""+i);
					list2.add(counter, name2+""+j);
					list3.add(counter, name3+""+k);

					counter++;
				}
			}
		}//end of the creation

		//now print everything
		Path outPath = Paths.get(this.outputFile);
		try(BufferedWriter writer = Files.newBufferedWriter(outPath, UsefulConstants.CHARSET_ENCODING)) {
			//print g1, g2 and g3

			writer.write("g1 ={ '" + list1.get(0) + "'");
			for(int i = 1; i < list1.size(); ++i) {
				writer.write("; '" + list1.get(i) + "'");
			}
			writer.write("};");
			writer.newLine();

			//now do the other two
			writer.write("g2 = {'" + list2.get(0) + "'");
			for(int i = 1; i < list2.size(); ++i) {
				writer.write("; '" + list2.get(i) + "'");
			}
			writer.write("};");
			writer.newLine();

			writer.write("g3 = {'" + list3.get(0) + "'");
			for(int i = 1; i < list3.size(); ++i) {
				writer.write("; '" + list3.get(i) + "'");
			}
			writer.write("};");
			writer.newLine();



		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** Prints in the outputFIle field of the object 3
	 * arrays. These are the combinations
	 * of a 3 ways ANOVA.
	 * */
	public void printSetupFileFor3WaysAnova() {
		int everythingIsNotEnough = level1.length * level2.length * level3.length;
		int counter = 0;

		List<String> list1 = new ArrayList<String>( everythingIsNotEnough );
		List<String> list2 = new ArrayList<String>( everythingIsNotEnough );
		List<String> list3 = new ArrayList<String>( everythingIsNotEnough );

		//we prepare the lists of string in a way they can be written down
		for(int i = 0; i < level1.length; ++i) {
			for(int j = 0; j < level2.length; ++j) {
				for(int k = 0; k < level3.length; ++k ) {
					list1.add(counter, level1[i]);
					list2.add(counter, level2[j]);
					list3.add(counter, level3[k]);

					counter++;
				}
			}
		}//end of the creation

		//now print everything
		Path outPath = Paths.get(this.outputFile);
		try(BufferedWriter writer = Files.newBufferedWriter(outPath, UsefulConstants.CHARSET_ENCODING)) {
			//print g1, g2 and g3

			writer.write("g1 ={ '" + list1.get(0) + "'");
			for(int i = 1; i < list1.size(); ++i) {
				writer.write("; '" + list1.get(i) + "'");
			}
			writer.write("};");
			writer.newLine();

			//now do the other two
			writer.write("g2 = {'" + list2.get(0) + "'");
			for(int i = 1; i < list2.size(); ++i) {
				writer.write("; '" + list2.get(i) + "'");
			}
			writer.write("};");
			writer.newLine();

			writer.write("g3 = {'" + list3.get(0) + "'");
			for(int i = 1; i < list3.size(); ++i) {
				writer.write("; '" + list3.get(i) + "'");
			}
			writer.write("};");
			writer.newLine();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/** This method prepares a file with the vectors required by the 
	 * ANOVA algorithm in MATLAB. These are the array describing
	 * the different combinations of our tests.
	 * */
	public void printSetupFileFor2WayAnova(String name1, int rep1,
			String name2, int rep2) {
		//the total number of rows we have
		int numRows = rep1 * rep2;

		int counter = 0;

		//lists to keep the data. THey will be later printed
		List<String> list1 = new ArrayList<String>( numRows );
		List<String> list2 = new ArrayList<String>( numRows );

		for(int j = 1; j <= rep1; ++j) {//the algorithms
			for(int k = 0; k < rep2; ++k ) {//the lambda
				list1.add(counter, name1+""+j);
				list2.add(counter, name2+""+k);

				counter++;
			}
		}//end first for

		//now print everything
		Path outPath = Paths.get(this.outputFile);
		try(BufferedWriter writer = Files.newBufferedWriter(outPath, UsefulConstants.CHARSET_ENCODING)) {
			//print the vectors
			writer.write("g2 = {'" + list1.get(0) + "'");
			for(int i = 1; i < list1.size(); ++i) {
				writer.write("; '" + list1.get(i) + "'");
			}
			writer.write("};");
			writer.newLine();

			writer.write("g3 = {'" + list2.get(0) + "'");
			for(int i = 1; i < list2.size(); ++i) {
				writer.write("; '" + list2.get(i) + "'");
			}
			writer.write("};");
			writer.newLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** It uses level 2 and level 3*/
	public void printSetupFileFor2WaysAnova() {
		int numRows = level2.length * level3.length;
		int counter = 0;

		//lists to keep the data. THey will be later printed
		List<String> list1 = new ArrayList<String>( numRows );
		List<String> list2 = new ArrayList<String>( numRows );

		for(int j = 0; j < level2.length; ++j) {//the algorithms
			for(int k = 0; k < level3.length; ++k ) {//the lambda
				list1.add(counter, level2[j]);
				list2.add(counter, level3[k]);

				counter++;
			}
		}//end first for

		//now print everything
		Path outPath = Paths.get(this.outputFile);
		try(BufferedWriter writer = Files.newBufferedWriter(outPath, UsefulConstants.CHARSET_ENCODING)) {
			//print the vectors
			writer.write("g2 = {'" + list1.get(0) + "'");
			for(int i = 1; i < list1.size(); ++i) {
				writer.write("; '" + list1.get(i) + "'");
			}
			writer.write("};");
			writer.newLine();

			writer.write("g3 = {'" + list2.get(0) + "'");
			for(int i = 1; i < list2.size(); ++i) {
				writer.write("; '" + list2.get(i) + "'");
			}
			writer.write("};");
			writer.newLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/** Test main*/
	public static void main(String[] args) {
		//set the path of the output file
		String outputPath = "/Users/dennisdosso/Desktop/ANOVA tests/setup_anova_big_all.m";
		SetupAnovaFile execution = new SetupAnovaFile(outputPath);


		String[] l1 = {"rLinkedMDB", "LinkedMDB", "rIMDB", "IMDB"};
		String[] l2 = {"TSA+BM25", "TSA+VDP"};
		String[] l3 = {"λ0", "λ1", "λ2", "λ3", "λ4", "λ5", "λ6", "λ7", "λ8", "λ9"};

		execution.setLevel1(l1);
		execution.setLevel2(l2);
		execution.setLevel3(l3);

		execution.printSetupFileFor3WaysAnova();
		
		/*
		outputPath = "/Users/dennisdosso/Desktop/ANOVA tests/setup_anova_2_ways.m";
		execution.setOutputFile(outputPath);
		execution.printSetupFileFor2WaysAnova();
		
		outputPath = "/Users/dennisdosso/Desktop/ANOVA tests/setup_anova_2_ways_time.m";
		execution.setOutputFile(outputPath);
		execution.setLevel2(l1);
		execution.setLevel3(l2);
		execution.printSetupFileFor2WaysAnova();
		*/


		System.out.println("we are done");

	}


	public String getOutputFile() {
		return outputFile;
	}


	public void setOutputFile(String outputFile) {
		this.outputFile = outputFile;
	}


	public String[] getLevel1() {
		return level1;
	}


	public void setLevel1(String[] level1) {
		this.level1 = level1;
	}


	public String[] getLevel2() {
		return level2;
	}


	public void setLevel2(String[] level2) {
		this.level2 = level2;
	}


	public String[] getLevel3() {
		return level3;
	}


	public void setLevel3(String[] level3) {
		this.level3 = level3;
	}



}
