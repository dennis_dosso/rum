package it.unipd.dei.ims.rum.indexing;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.terrier.indexing.Collection;
import org.terrier.indexing.TRECCollection;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;
import org.terrier.structures.indexing.classical.BasicIndexer;
import org.terrier.utility.ApplicationSetup;

import it.unipd.dei.ims.rum.utilities.PathUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** Class to index a directory containing TREC files. 
 * */
@Deprecated
public class RumTRECIndexer {

	/**The directory with the files to index.
	 * */
	private String directoryToIndex;
	
	/** Path to the terriere home, the directory of Terrier which we want to use*/
	private String terrier_home;
	/** Path to the terrier/etc directory, the directory containing the file terrier.properties which
	 * describes the behaviour of Terrier.*/
	private String terrier_etc;
	
	/** String containing the path where we want to save the index.*/
	private String indexPath;
	
	/** Method that executes the indexing
	 * */
	public void index(String flag) {
		
		//set the needed properties
		System.setProperty("terrier.home", terrier_home);
		System.setProperty("terrier.etc", terrier_etc);
		
		//set what kind of indexing we want
		if(flag.equals("unigram"))
			System.setProperty("tokeniser", "EnglishTokeniser");
		else if(flag.equals("bigram")) {
			ApplicationSetup.setProperty("tokeniser", "BigramTokeniser");
			System.setProperty("tokeniser", "BigramTokeniser");
		}
		
		//get the files that we want to index
		List<String> files = PathUsefulMethods.getListOfFiles(directoryToIndex);
		//indexer with the path of the directory where to index
		BasicIndexer indexer = new BasicIndexer(indexPath, "data");
		Collection coll = new TRECCollection(files);
		
		indexer.index(new Collection[]{ coll });
		System.out.println("Index created");
		
		//open the new index
		Index index = IndexOnDisk.createIndex(indexPath, "data");
		System.out.println("We have indexed " + index.getCollectionStatistics().getNumberOfDocuments() + " documents, with number of"
				+ " tokens: " 
				+ index.getCollectionStatistics().getNumberOfTokens());
	}

	//#####################################
	
	public static void main(String[] args) throws IOException {
		RumTRECIndexer idx = new RumTRECIndexer();
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/indexing.properties");
		
		//set the parameters
		idx.setTerrier_home(map.get("terrier.home"));
		idx.setTerrier_etc(map.get("terrier.etc"));
		idx.setDirectoryToIndex(map.get("directory.to.index"));
		idx.setIndexPath(map.get("index.path"));
		
		//index
		idx.index("unigram");
	}
	
	//#####################################

	public String getDirectoryToIndex() {
		return directoryToIndex;
	}


	public void setDirectoryToIndex(String directoryToIndex) {
		this.directoryToIndex = directoryToIndex;
	}


	public String getTerrier_home() {
		return terrier_home;
	}


	public void setTerrier_home(String terrier_home) {
		this.terrier_home = terrier_home;
	}


	public String getTerrier_etc() {
		return terrier_etc;
	}


	public void setTerrier_etc(String terrier_etc) {
		this.terrier_etc = terrier_etc;
	}


	public String getIndexPath() {
		return indexPath;
	}


	public void setIndexPath(String indexPath) {
		this.indexPath = indexPath;
	}
}
