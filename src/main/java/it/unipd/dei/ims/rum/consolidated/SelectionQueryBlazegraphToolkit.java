package it.unipd.dei.ims.rum.consolidated;

import java.io.IOException;
import java.util.Map;

import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;

import it.unipd.dei.ims.rum.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/**This class uses the properties file SelectionQueryBlazegraphTookit.properties*/
public class SelectionQueryBlazegraphToolkit {

	
	/** Path of the RDF database to interrogate*/
	private String rdfDatabasePath; 
	
	/** SPARQL query. You can write it without the use of prefixes.*/
	private String sparqlQuery; 
	
	/** String with the prefix you want to use with
	 * the SPARQL query.
	 * */
	private String prefixString;
	
	/** String with the output .txt file where to write the results of the query
	 * */
	private String outputFilePath;
	
	public SelectionQueryBlazegraphToolkit() {
		
	}
	
	/** Fill the field with the information contained in the
	 * property file passed as parameter.
	 * @throws IOException 
	 * */
	public void setupFromPropertyFile(String propertyFilePath) throws IOException {
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap(propertyFilePath);
		
		this.rdfDatabasePath = map.get("rdf.database.path");
		this.prefixString = map.get("prefix.string");
		this.outputFilePath = map.get("output.file.path");
		this.sparqlQuery = map.get("sparql.query");
	}
	
	
	public void executeQuerySPARQL() {
		
		//open the connection to the database
		Repository repo = BlazegraphUsefulMethods.getRepositoryFromPath(this.rdfDatabasePath);
		
		try {
			repo.initialize();
			RepositoryConnection cxn = BlazegraphUsefulMethods.getRepositoryConnection(repo);
			
			//prepare the query
			final TupleQuery tq = cxn.prepareTupleQuery(QueryLanguage.SPARQL, prefixString + " " + this.sparqlQuery);
			
			//evaluate it
			TupleQueryResult res = tq.evaluate();
			
			int counter = 0;
			//read each triple of the result
			while (res.hasNext()) {
				//for each triple result
				
				counter++;
				
				BindingSet bindingSet = res.next();
				System.out.println(counter + ": " + bindingSet);
			}
			System.out.println("triples: " + counter);
			
			res.close();
			cxn.close();
			
		} catch (RepositoryException e) {
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		} finally {
			try {
				repo.shutDown();
			} catch (RepositoryException e) {
				e.printStackTrace();
			}
		}
	}

	/**Test main
	 * */
	public static void main(String[] args) {
		SelectionQueryBlazegraphToolkit phase = new SelectionQueryBlazegraphToolkit();
		try {
			phase.setupFromPropertyFile("properties/SelectionQueryBlazegraphToolkit.properties");
			
			phase.executeQuerySPARQL();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getRdfDatabasePath() {
		return rdfDatabasePath;
	}


	public void setRdfDatabasePath(String rdfDatabasePath) {
		this.rdfDatabasePath = rdfDatabasePath;
	}


	public String getSparqlQuery() {
		return sparqlQuery;
	}


	public void setSparqlQuery(String sparqlQuery) {
		this.sparqlQuery = sparqlQuery;
	}


	public String getPrefixString() {
		return prefixString;
	}


	public void setPrefixString(String prefixString) {
		this.prefixString = prefixString;
	}


	public String getOutputFilePath() {
		return outputFilePath;
	}


	public void setOutputFilePath(String outputFilePath) {
		this.outputFilePath = outputFilePath;
	}
	
}
