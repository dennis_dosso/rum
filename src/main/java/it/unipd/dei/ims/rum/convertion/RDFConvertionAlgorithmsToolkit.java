package it.unipd.dei.ims.rum.convertion;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.jena.ext.com.google.common.base.Stopwatch;
import org.openrdf.model.Value;
import org.openrdf.query.BindingSet;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFParseException;

import it.unipd.dei.ims.rum.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PathUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** Blanco algorithm, phase 0.2
 * <p>
 * OFFLINE, this computation should be done only once for each graph.
 * This operation is common to all algorithms, not only Blanco.
 * <p>
 * This algorithm reads all the file inside a directory and 
 * utilize the Blazegraph directory to transform them in a TDB repository.
 * <p>
 * The data are read from the properties/convertion.properties file.
 * 
 * */
public class RDFConvertionAlgorithmsToolkit {
	/** List of strings representing the paths of the file we want to save in a storage*/
	private List<String> paths;
	
	/** The format of the RDF file. Default to turtle. 
	 * We support for now turtle and n3.
	 * <p>
	 * property: language
	 * */
	private String language;
	
	/** String to connet to the PostreSQL DB.
	 * property: jdbc.connection.string
	 * */
	private String jdbcConnectingString;
	
	public final static String TURTLE = "turtle";
	public final static String N3 = "n3";
	
	/** Directory where the RDF files to be inserted in the
	 * database are located.
	 * <p>
	 * property: file.to.convert.directory
	 * */
	private String fileToConvertDirectory;
	
	/** SQL command to be performed when inserting the values into the database.
	 * */
	private String SQL_INSERT_TRIPLE = "INSERT INTO public.triple_store(" + 
			"	subject_, predicate_, object_)" + 
			"	VALUES (?, ?, ?);";
	
	/** Path of the triple store database where to read/write information.
	 * <p>
	 * property: tdb.database.path*/
	private String tdbDatabasePath;
	
	/** Prepares all the fields with the information contained in the property file.
	 * */
	public RDFConvertionAlgorithmsToolkit() {
		try {
			this.setup();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(language==null)
			language = TURTLE;
	}
	
	public void setup() throws IOException {
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/convertion_properties");
		language = map.get("language");
		jdbcConnectingString = map.get("jdbc.connection.string");
		fileToConvertDirectory = map.get("file.to.convert.directory");
		tdbDatabasePath = map.get("tdb.database.path");
		
		this.initializePaths(fileToConvertDirectory);
	}
	
	/** Given a directory containing files, this method 
	 * initializes the paths field with all the files in that directory. 
	 * We suppose that the directory only contains file RDF
	 * in some format (e.g. turtle o .nt).
	 * */
	public void initializePaths(String directory) {
		paths = PathUsefulMethods.getListOfFiles(directory);
	}
	
	/** Converts all the files contained in the paths field
	 * in a RDB repository with Blazegraph.
	 * 
	 * @param outputFile where to save the .jnl file
	 * */
	public void fromTextFileToRDFTripleStorePoweredByBlazegraph() {
		//open the database
		final Repository repo = BlazegraphUsefulMethods.createRepository(this.tdbDatabasePath);
		RepositoryConnection cxn = null;
		
		try {
			repo.initialize();
			Stopwatch timer = Stopwatch.createStarted();
			//open the connection to the repository
			cxn = repo.getConnection();
			//for every file
			for(String path : this.paths) {
				System.out.println("Now loading file " + path);
				cxn.begin();
				if(language.equals(N3))
					cxn.add(new File(path), null, org.openrdf.rio.RDFFormat.N3 );
				else if(language.equals(TURTLE))
					cxn.add(new File(path), null, org.openrdf.rio.RDFFormat.TURTLE );
				System.out.println("File read in " + timer);
				System.out.println("File read in " + timer);
				timer.reset().start();
				System.out.println("Now committing...");
				cxn.commit();
				System.out.println("Commit done in " + timer);
				timer.reset().start();
			}
			timer.stop();
			System.out.print("All the files have been incorporated in the triple store");
		} catch (RepositoryException e) {
			e.printStackTrace();
		} catch (RDFParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				cxn.close();
				repo.shutDown();
			} catch (RepositoryException e) {
				e.printStackTrace();
			}
		}
	}
	
	/** Utilizes the information contained in a Triple Store to populate a 
	 * table in a RDB with the information about the triples of a graph.
	 * 
	 * */
	public void fromRDFTripleStoreToRDBTripleStorePoweredByBlazegrapg() {
		Repository repository = BlazegraphUsefulMethods.getRepositoryFromPath(this.tdbDatabasePath);
		RepositoryConnection connection = null;
		TupleQueryResult iterator = null;
		
		Connection rdbConnection = null;
		
		try {
			//open RDB connection
			rdbConnection = DriverManager.getConnection(jdbcConnectingString);
			
			//prepare the insert statement
			PreparedStatement preparedInsert = rdbConnection.prepareStatement(this.SQL_INSERT_TRIPLE);
			
			//get the rdf database
			repository.initialize();
			connection = BlazegraphUsefulMethods.getRepositoryConnection(repository);
			//get the iterator
			iterator = BlazegraphUsefulMethods.getIterator(connection);
			
			int progressiveCounter = 0;
			int totalCounter = 0;
			
			Stopwatch timer = Stopwatch.createStarted();
			
			while(iterator.hasNext()) {
				//get the triple
				BindingSet triple = iterator.next();
				//get the three amigos
				Value subject = triple.getValue("s");
				Value predicate = triple.getValue("p");
				Value object = triple.getValue("o");
				//insert them into the database
				preparedInsert.setString(1, subject.toString());
				preparedInsert.setString(2, predicate.toString());
				preparedInsert.setString(3, object.toString());
				
				preparedInsert.addBatch();
				progressiveCounter++;
				
				if( progressiveCounter>=100000 ) {
					totalCounter+=progressiveCounter;
					progressiveCounter = 0;
					
					preparedInsert.executeBatch();
					System.out.println("Inserted ... " + totalCounter + " triples in " + timer);
					
					preparedInsert.clearBatch();
					timer.stop().reset().start();
				}
			}
			if( progressiveCounter>=0 ) {
				totalCounter+=progressiveCounter;
				progressiveCounter = 0;
				
				preparedInsert.executeBatch();
				System.out.println("Last update ... " + totalCounter + " triples in " + timer);
				
				preparedInsert.clearBatch();
				timer.stop().reset().start();
			}
			iterator.close();
			
		} catch (RepositoryException e) {
			System.err.print("Error in repository init");
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if(connection!=null) {
				try {
					connection.close();
					repository.shutDown();
				} catch (RepositoryException e) {
					e.printStackTrace();
				}
			}
			if(rdbConnection!=null) {
				try {
					rdbConnection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
}
