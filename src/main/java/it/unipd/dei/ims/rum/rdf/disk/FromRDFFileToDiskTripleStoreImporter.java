package it.unipd.dei.ims.rum.rdf.disk;

import java.io.File;
import java.io.IOException;

import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFParseException;

import it.unipd.dei.ims.rum.utilities.BlazegraphUsefulMethods;

/** This is a simple class to use to read the one RDF file. 
 * 
 * 
 * */
public class FromRDFFileToDiskTripleStoreImporter {

	public FromRDFFileToDiskTripleStoreImporter() {
		
	}
	
	/** Reads a text file in N3 syntax containing
	 * an RDF graph and inserts it in a triple store
	 * 
	 * @param n3FilePath path of the n3 file to read and import
	 * @param outputRepositoryPath the path of the repository on disk
	 * where to write the dataset. */
	public void importFromN3toDiskRepository(String n3FilePath, String outputRepositoryPath) {
		RepositoryConnection cxn = null;
		//create the repo and initialize it
		Repository repo = BlazegraphUsefulMethods.createRepository(outputRepositoryPath);
		try {
			repo.initialize();
			
			//open the connection to the repository
			cxn = repo.getConnection();
			//begin transaction
			cxn.begin();
			//add the file to the repository - here we specify the N3 syntax
			cxn.add(new File(n3FilePath), null, org.openrdf.rio.RDFFormat.N3);
			//end transaction
			cxn.commit();
		} catch (RepositoryException e) {
			e.printStackTrace();
		} catch (RDFParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/** Reads a text file in Turtle syntax containing
	 * an RDF graph and inserts it in a triple store
	 * 
	 * @param filePath path of the turtle file to read and import
	 * @param outputRepositoryPath the path of the repository on disk
	 * where to write the dataset. */
	public void importFromTurtletoDiskRepository(String filePath, String outputRepositoryPath) {
		RepositoryConnection cxn = null;
		//create the repo and initialize it
		Repository repo = BlazegraphUsefulMethods.createRepository(outputRepositoryPath);
		try {
			repo.initialize();
			
			//open the connection to the repository
			cxn = repo.getConnection();
			//begin transaction
			cxn.begin();
			//add the file to the repository - here we specify the N3 syntax
			cxn.add(new File(filePath), null, org.openrdf.rio.RDFFormat.TURTLE);
			//end transaction
			cxn.commit();
		} catch (RepositoryException e) {
			e.printStackTrace();
		} catch (RDFParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	
	/** test main*/
	public static void main(String[] args) {
		FromRDFFileToDiskTripleStoreImporter importer = new FromRDFFileToDiskTripleStoreImporter();
		
		//two hardcoded strings, maybe I will use a property file one day. One day...
		
		//path of the database
		String filePath = "/Users/dennisdosso/Documents/RDF_DATASETS/IUPHAR/dataset/gtp-rdf.n3";
		String rdfRepository = "/Users/dennisdosso/Documents/RDF_DATASETS/IUPHAR/dataset/gtp.jnl";
		importer.importFromN3toDiskRepository(filePath, rdfRepository);
		
		System.out.print("done");
	}
}
