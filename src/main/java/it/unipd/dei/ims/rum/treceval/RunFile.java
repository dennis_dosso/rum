package it.unipd.dei.ims.rum.treceval;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import it.unipd.dei.ims.rum.utilities.UsefulMathFunctions;

/** This class represent a run file
 * */
public class RunFile {

	/** Every entry in the map represents one query ID,
	 * the list of elements are the documents.
	 * */
	private Map<String, List<RunElement>> map;

	//private List<Double> apList, P1List, P5List, P10List, P20List, P50List, P100List, P1000List,
	//recallList, ndcg10List, ndcg100List;

	
	private double MAP100, mp1, mp10, mp5, mp100, mp1000, mrecall1, mrecall5,
	mrecall10, mrecall100, mrecall1000, mndcg10, mndcg100;

	public RunFile() {
		map = new HashMap<String, List<RunElement>>();
		
		
		MAP100 = 0;
		mp1 = 0; 
		mp10 = 0;
		mp5 = 0;
		mp10 = 0;
		mp100 = 0;
		mp1000 = 0; 
		mrecall1 = 0;
		mrecall5 = 0;
		mrecall10 = 0;
		mrecall100 = 0;
		mrecall1000 = 0;
		mndcg10 = 0; 
		mndcg100 = 0;
		
//		apList = new ArrayList<Double>();
//		P1List= new ArrayList<Double>();
//		P5List = new ArrayList<Double>();
//		P10List = new ArrayList<Double>();
//		P20List = new ArrayList<Double>();
//		P50List = new ArrayList<Double>();
//		P100List = new ArrayList<Double>(); 
//		P1000List = new ArrayList<Double>();
//		recallList = new ArrayList<Double>();
//		ndcg10List = new ArrayList<Double>(); 
//		ndcg100List = new ArrayList<Double>();
	}

	/** Inserts a new element in the file. */
	public void addElement(RunElement entry) {
		//take the query id of this entry
		String queryId = entry.getQueryId();
		if(! this.map.containsKey(queryId)) {
			//a new query
			//create a new list of entries
			List<RunElement> list = new ArrayList<RunElement>();
			list.add(entry);
			this.map.put(queryId, list);
		} else {
			List<RunElement> list  = map.get(queryId);
			list.add(entry);
			this.map.put(queryId, list);
		}
	}

	/** Given the runFile, provided that the elements in it  
	 * have their rank and their relevance judgments, executes
	 * the evaluations, computes the metrics and prints them.
	 * <p>
	 * NB: we do not need here the qrels since it is supposed
	 * that the entries in the RunFile already have their rank 
	 * and their relevance judgments
	 * */
	public void runEvaluation(String outputFile, QRelsFile qRel) {

		//open the writer in append option
		try(FileWriter fw = new FileWriter(outputFile, false);
				BufferedWriter writer = new BufferedWriter(fw);) {
			
			int denominator = 0;
			int totalQueries = qRel.getTotalNumberOfQueries();

			for(Entry<String, List<RunElement>> e : this.map.entrySet()) {
				this.runEvaluationForOneQuery(writer, e.getValue(), qRel);
				denominator++;
			}
			
			//now we write the averages
			this.runAverageEvaluation(writer, denominator, totalQueries);

		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}
	
	private void runAverageEvaluation(BufferedWriter writer, int denominator, int tot) {
		MAP100 = (double) MAP100 / denominator;
		mp1 = (double) mp1 / denominator; 
		mp10 = (double) mp10 / denominator;
		mp5 = (double) mp5 / denominator;
		mp100 = (double) mp100 / denominator;
		mp1000 = (double) mp1000 / denominator; 
		mrecall1 = (double) mrecall1 / denominator;
		mrecall5 = (double) mrecall5 / denominator;
		mrecall10 = (double) mrecall10 / denominator;
		mrecall100 = (double) mrecall100 / denominator;
		mrecall1000 = (double) mrecall1000 / denominator;
		mndcg10 = (double) mndcg10 / denominator; 
		mndcg100 = (double) mndcg100 / denominator;
		
		try {
			writer.write("\n\nAVERAGES\n");
			
			writer.write("recall@1: " + mrecall1 + "\n");
			writer.write("recall@5: " + mrecall5 + "\n");
			writer.write("recall@10: " + mrecall10 + "\n");
			writer.write("recall@100: " + mrecall100 + "\n");
			writer.write("recall@1000: " + mrecall1000 + "\n");
			writer.write("prec@1: " + mp1 + "\n");
			writer.write("prec@5: " + mp5 + "\n");
			writer.write("prec@10: " + mp10 + "\n");
			writer.write("prec@100: " + mp100 + "\n");
			writer.write("prec@1000: " + mp1000 + "\n");
			writer.write("MAP@100: " + MAP100 + "\n");
			writer.write("NDCG@10: " + mndcg10 + "\n");
			writer.write("NDCG@100: " + mndcg100 + "\n");
			writer.write("\n**********\n");
			
			writer.write("total queries in the run file: " + denominator + 
					" total queries in the qrels: " + tot);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}



	private void runEvaluationForOneQuery(BufferedWriter writer, List<RunElement> docs, QRelsFile qRel) {
		//get the total number of relevant docouments for this query
		String queryId = docs.get(0).getQueryId();
		int totalRecall = qRel.getTotalNumberOfRelevantDocsForOneQuery(queryId);

		//it works as cumulative gain if we think about only 0/1 as judgments
		int counter = 0;
		//discounted cumulative gain
		double dcg = 0;

		double[] idcg = qRel.computeIDCG(queryId);
		double idcg10 = idcg[0];
		double idcg100 = idcg[1];

		//XXX
		double precAt1 = 0, precAt5 = 0, 
				precAt10 = 0, precAt20 = 0, 
				precAt50 = 0, precAt100 = 0, 
				precAt1000 = 0, recall1 = 0, 
				recall5 = 0, recall10 = 0,
				recall100 = 0, recall1000 = 0;
		double averagePrecision = 0;
		//average precision @ 100
		double ap100 = 0;
		double dcg10  = 0, dcg100 = 0;

		//for every document on the list
		for(int i = 1; i <= docs.size(); ++i) {
			RunElement doc = docs.get(i - 1);
			int relevance = doc.getRelevance();
			int multiplier = 0;
			if(relevance > 0) {
				counter++;
				multiplier = 1;
			}

			//working on ndcg
			double cumulativeGain = 0;
			if(i <= 2) {
				cumulativeGain = relevance;
			} else {
				cumulativeGain = (double) relevance / UsefulMathFunctions.logBase2Of(i);
			}
			//update the sum for the discounted cumulative gain
			dcg += cumulativeGain;


			//computing average precision
			//compute the inner ratio (summatory divided by index i)
			double innerRatio = (double) counter / i;
			//multiply by 0 or 1
			innerRatio *= multiplier;
			averagePrecision += innerRatio;

			//now working on precision
			if(i == 1) {
				precAt1 = counter;
				recall1 = (double) counter / totalRecall;
			} else if(i == 5) {
				precAt5 = (double) counter / 5;
				recall5 = (double) counter / totalRecall;
			} else if(i == 10) {
				precAt10 = (double) counter / 10;
				dcg10 = dcg;
				recall10 = (double) counter / totalRecall;
			} else if(i == 20) {
				precAt20 = (double) counter / 20;
			} else if(i == 50) {
				precAt50 = (double) counter / i;
			} else if(i == 100) {
				precAt100 = (double) counter / i;
				dcg100 = dcg;
				recall100 = (double) counter/ totalRecall;
				ap100 = averagePrecision;
			} else if (i == 1000) {
				precAt1000 = (double) counter / i;
				recall1000 = (double) counter / totalRecall;
			}
		}//analyzed all the elements in the run
		
		//cover some special cases due having less than 1000 elements
		if(docs.size() < 5) {
			precAt5 = counter / 5;
			recall5 = (double) counter / totalRecall;
		}
		if(docs.size() < 10) {
			precAt10 = counter / 10;
			dcg10 = dcg;
			recall10 = (double) counter / totalRecall;
		} 
		if(docs.size() < 100) {
			precAt100 = counter / 100;
			dcg100 = dcg;
			ap100 = averagePrecision;
			recall100 = (double) counter / totalRecall;
		} 
		if(docs.size() < 1000) {
			precAt1000 = counter / 1000;
			recall1000 = (double) counter / totalRecall;
		} 
		
		
		
		//computing recall
		double recall = (double) counter / totalRecall;

		//complete the computation of average precision
		averagePrecision = (double) averagePrecision / totalRecall;
		ap100 = (double) ap100 / totalRecall;

		double ndcg10 = (double) dcg10 / idcg10;
		double ndcg100 = (double) dcg100 / idcg100;

		//now we write something
		try {
			writer.write("QUERY: " + queryId + "\n\n");
			writer.write("Recall Base: " + totalRecall + "\n");
			writer.write("recall@1: " + recall1 + "\n");
			writer.write("recall@5: " + recall5 + "\n");
			writer.write("recall@10: " + recall10 + "\n");
			writer.write("recall@100: " + recall100 + "\n");
			writer.write("recall@1000: " + recall1000 + "\n");
			writer.write("prec@1: " + precAt1 + "\n");
			writer.write("prec@5: " + precAt5 + "\n");
			writer.write("prec@10: " + precAt10 + "\n");
			writer.write("prec@100: " + precAt100 + "\n");
			writer.write("prec@1000: " + precAt1000 + "\n");
			writer.write("AP@100: " + ap100 + "\n");
			writer.write("AP@1000 " + averagePrecision + "\n");
			writer.write("NDCG@10: " + ndcg10 + "\n");
			writer.write("NDCG@100: " + ndcg100 + "\n");
			writer.write("\n**********\n\n");
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		MAP100 += ap100;
		mp1 += precAt1; 
		mp10 += precAt10;
		mp5 += precAt5;
		mp100 += precAt100;
		mp1000 += precAt1000; 
		mrecall1 += recall1;
		mrecall5 += recall5;
		mrecall10 += recall10;
		mrecall100 += recall100;
		mrecall1000 += recall1000;
		mndcg10 += ndcg10; 
		mndcg100 += ndcg100;
	}
}
