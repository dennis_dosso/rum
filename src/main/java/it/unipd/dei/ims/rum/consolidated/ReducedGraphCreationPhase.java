package it.unipd.dei.ims.rum.consolidated;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import org.apache.jena.ext.com.google.common.base.Stopwatch;
import org.openrdf.model.Model;
import org.openrdf.model.Statement;
import org.openrdf.model.Value;
import org.openrdf.model.impl.TreeModel;
import org.openrdf.query.BindingSet;
import org.openrdf.query.GraphQuery;
import org.openrdf.query.GraphQueryResult;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;

import it.unipd.dei.ims.datastructure.RDFSimpleNode;
import it.unipd.dei.ims.rum.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.SQLUtilities;

/** Creation of a subgraph of another graph with a reduced number of triples
 * */
public class ReducedGraphCreationPhase {

	/** Path of the original graph.*/
	private String originalGraphDatabasePath;

	/** path of the output reduced graph*/
	private String reducedGraphPath;

	/** The minimum number of triples that the 
	 * reduced graph should have.
	 * */
	private int maxNumberOfTriples;

	private int radius;

	private int limit;

	private int step;

	/** jdbc string to connect to the database with the full graph*/
	private String jdbcOriginalDatabase;

	/** jdbc string to connect to the new database*/
	private String jdbcReducedDatabase;

	/** SQL command to read  triples from the database */
	private static final String SQL_READ_TRIPLES = "SELECT subject_, predicate_, object_ from triple_store "
			+ "order by id_ limit ? offset ?;";

	private static final String SQL_INSERT_TRIPLES = "INSERT INTO public.triple_store(" + 
			"subject_, predicate_, object_)" + 
			"	VALUES (?, ?, ?);";

	private int maxIterationNumber;


	/** clean the table before an insertion. Pay great attention here fella.
	 * */
	private static final String SQL_TRUNCATE_TABLE = "truncate table triple_store";

	public ReducedGraphCreationPhase () {

	}

	public void setup(String propertiesFile) throws IOException {
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap(propertiesFile);

		this.originalGraphDatabasePath = map.get("original.graph.database.path");
		this.reducedGraphPath = map.get("reduced.graph.path");
		this.maxNumberOfTriples = Integer.parseInt(map.get("max.number.of.triples"));
		this.limit = Integer.parseInt(map.get("limit"));
		this.step = Integer.parseInt(map.get("step"));
		this.jdbcOriginalDatabase = map.get("original.jdbc.connection.string");
		this.jdbcReducedDatabase = map.get("reduced.jdbc.connection.string");
		this.radius = Integer.parseInt(map.get("radius"));
		this.maxIterationNumber = Integer.parseInt(map.get("max.iteration.number"));

	}

	@Deprecated
	public void createAReducedGraph() {

		//open the connection to the original database
		Repository repo = BlazegraphUsefulMethods.getRepositoryFromPath(this.originalGraphDatabasePath);

		Model graph = new TreeModel();

		int offSet = 0;

		try {
			repo.initialize();

			RepositoryConnection cxn = BlazegraphUsefulMethods.getRepositoryConnection(repo);
			Stopwatch timer = Stopwatch.createStarted();

			while(true) {
				//string with the query
				String sparqlQuery = "CONSTRUCT where {?s ?p ?o} limit " + this.limit + " offset " + offSet;
				//update the offset in order to move the window over the graph
				offSet += step;

				//query to create a graph
				GraphQuery graphQuery = cxn.prepareGraphQuery(QueryLanguage.SPARQL,  sparqlQuery);

				//evaluate the query
				GraphQueryResult result = graphQuery.evaluate();

				//reached the end of the database
				if(!result.hasNext())
					break;

				while(result.hasNext()) {
					//get the triple and add it to the graph
					Statement t = result.next();
					graph.add(t);
				}

			}

			//end of the creation of the graph, now save it in a triple store repository
			//			BlazegraphUsefulMethods.printTheDamnGraph(graph, this.reducedGraphPath);

			cxn.close();
			repo.shutDown();

			repo = BlazegraphUsefulMethods.getRepositoryFromPath(this.reducedGraphPath);
			repo.initialize();
			cxn = repo.getConnection();

			cxn.begin();
			cxn.add(graph);
			cxn.commit();

			cxn.close();


		} catch (RepositoryException e) {
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		} finally {
			try {
				repo.shutDown();
			} catch (RepositoryException e) {
				e.printStackTrace();
			}
		}

	}

	/** Recommended when you are transalting from a big graph in order not to keep everything
	 * in RAM while sampling.
	 * <p>
	 * Still seen to be incrementally slow, probably due
	 * to the SPARQL select query with offset. Try with RDB and
	 * good old SQL.
	 * 
	 * */
	public void createAReducedGraphFromBigGraph() {
		//open the connection to the original database
		Repository repo = BlazegraphUsefulMethods.getRepositoryFromPath(this.originalGraphDatabasePath);
		Repository reducedRepo = BlazegraphUsefulMethods.getRepositoryFromPath(this.reducedGraphPath);

		Model graph = new TreeModel();

		int offSet = 0;

		try {
			repo.initialize();
			reducedRepo.initialize();

			RepositoryConnection cxn = BlazegraphUsefulMethods.getRepositoryConnection(repo);
			RepositoryConnection reducedCxn = reducedRepo.getConnection();
			int size = 0;

			Stopwatch timer = Stopwatch.createStarted();

			while(true) {
				//string with the query
				String sparqlQuery = "CONSTRUCT where {?s ?p ?o} limit " + this.limit + " offset " + offSet;
				//update the offset in order to move the window over the graph
				offSet += step;

				//query to create a graph
				GraphQuery graphQuery = cxn.prepareGraphQuery(QueryLanguage.SPARQL,  sparqlQuery);

				//evaluate the query
				GraphQueryResult result = graphQuery.evaluate();

				//reached the end of the database
				if(!result.hasNext())
					break;

				while(result.hasNext()) {
					//get the triple and add it to the graph
					Statement t = result.next();
					graph.add(t);
					size++;
				}

				//add the group of triples to the reduced database
				if( size >= 10000 ) {
					System.out.println("updating the database");

					reducedCxn.begin();
					reducedCxn.add(graph);
					reducedCxn.commit();

					graph.clear();
					size=0;

					System.out.println("scanned " + offSet + " triples of the graph in " + timer);
					timer.reset();
				}


			}

			//end of the creation of the graph, now save it in a triple store repository
			cxn.close();
			repo.shutDown();

			//last update
			if(size>0) {
				System.out.print("updating the database");
				reducedCxn.begin();
				reducedCxn.add(graph);
				reducedCxn.commit();
			}

			reducedCxn.close();
			reducedRepo.shutDown();


		} catch (RepositoryException e) {
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		} finally {
			try {
				repo.shutDown();
			} catch (RepositoryException e) {
				e.printStackTrace();
			}
		}
	}

	public void createAReducedGraphInRDB () {
		//open the connections to the databases

		//offset that will be incremented
		int localOffset = 0;

		try {
			//connection to the database with the original graph saved in the triple_store directory
			Connection originalConnection = DriverManager.getConnection(jdbcOriginalDatabase);
			Connection reducedConnection = DriverManager.getConnection(jdbcReducedDatabase);

			Stopwatch timer = Stopwatch.createStarted();

			int counter = 0;
			PreparedStatement ps = reducedConnection.prepareStatement(SQL_INSERT_TRIPLES);
			while(true) {
				ResultSet rs = SQLUtilities.executeOffsetQuery(originalConnection, this.limit, localOffset, SQL_READ_TRIPLES);
				//update the offset
				localOffset += step;

				if(rs.next()) {
					rs.beforeFirst();
					while(rs.next()) {
						//get the data
						String subject = rs.getString("subject_");
						String predicate = rs.getString("predicate_");
						String object = rs.getString("object_");

						//prepare the insertion of the element in the second database
						ps.setString(1, subject);
						ps.setString(2, predicate);
						ps.setString(3, object);

						//add to batch
						ps.addBatch();
						counter++;
					}

					if(counter>=10000) {
						counter = 0;
						System.out.println("passed " + localOffset + " triples, now uploading...");

						ps.executeBatch();
						ps.clearBatch();

						System.out.println("database updated in " + timer);
						//						timer.reset();						
					}


				} else {
					//last insertion
					if(counter>0) {
						System.out.println("passed " + localOffset + " triples, now last uploading...");

						ps.executeBatch();
						ps.clearBatch();

						System.out.println("database updated in " + timer);
						timer.reset();						
					}
					System.out.print("done");
					//read all the database
					break;
				}
			}

			//be nice and clean
			originalConnection.close();
			reducedConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
			e.getNextException().printStackTrace();
			//			System.err.println(e.getNextException());
		}
	}

	/** Creates a reduced RDB triple store, but in a more connected way.
	 * This is a new algorithm that utilizes a simple
	 * heuristic to build up a graph which has more connected components.
	 * <p>
	 * This algorithm reads from a RDB triple store with the
	 * original (big database). This was due to the fact that Blazegraph
	 * becomes a little slow with the offset operation inside the select
	 * query. Maybe one day we will try Jena upon this subject (or maybe not).
	 * <p>
	 * The method builds a RDF repository which is a random sampling quite connected
	 * of the original big graph. We can import these informations inside 
	 * a RDB graph in a second moment with a code which is already written. 
	 * See {@link it.unipd.dei.ims.rum.convertion.RDFtoRDBTripleStoreConverter}
	 * */
	public void createAReducedGraphFroRDBtoRDFSophisticated () {

		//offset that will be incremented
		int localOffset = 0;

		try {
			//connection to the database with the original graph saved in the triple_store directory
			Connection originalConnection = DriverManager.getConnection(jdbcOriginalDatabase);
			Connection reducedConnection = DriverManager.getConnection(jdbcReducedDatabase);

			//get a RDF connection 
			Repository repo = BlazegraphUsefulMethods.getRepositoryFromPath(this.reducedGraphPath);
			repo.initialize();
			RepositoryConnection cxn = repo.getConnection();

			Stopwatch timer = Stopwatch.createStarted();
			long startTime = System.currentTimeMillis();

			Model graph = new TreeModel();

			//counter for the triples inside the graph
			//use this instead of calling graph.size(), which can become cumbersome sometimes
			int totalTripleCounter = 0;

			while( totalTripleCounter <= this.maxNumberOfTriples ) {//until we reached the required number of triples
				//read only 1 triple
				ResultSet rs = SQLUtilities.executeOffsetQuery(originalConnection, 1, localOffset, SQL_READ_TRIPLES);
				if(rs.next()) {
					//expand the subgraph
					graph.addAll(this.expandSubgraph(rs.getString("subject_"), originalConnection));
					localOffset += this.step;

					if(graph.size() >= 1000) {
						System.out.println("now updating after " + timer + " total number of overseen triples is: " + localOffset +
								" total number of triples inserted is: " + totalTripleCounter);
						long estimatedTime = System.currentTimeMillis() - startTime;
						System.out.println("est. time: " + estimatedTime);
						cxn.begin();
						cxn.add(graph);
						cxn.commit();
						
						//TODO to be absolutely accurate here we should count the size of the 
						//RDF DB
						try {
							//look into the DB and check the total number of triples
							final TupleQuery tq = cxn.prepareTupleQuery(QueryLanguage.SPARQL, "SELECT (count(*) AS ?count) { ?s ?p ?o .}");
							//evaluate it
							TupleQueryResult res = tq.evaluate();
							if(res.hasNext()) {
								BindingSet bindingSet = res.next();
								Value val = bindingSet.getValue("count");
								String v = val.stringValue();
								int graphSize = Integer.parseInt(v);
								totalTripleCounter = graphSize;
							} else {
								//a way to deal anyway with the thing
								totalTripleCounter += graph.size();
							}
						} catch (MalformedQueryException e) {
							totalTripleCounter += graph.size();
							e.printStackTrace();
						} catch (QueryEvaluationException e) {
							totalTripleCounter += graph.size();
							e.printStackTrace();
						}
						
						//totalTripleCounter += graph.size();

						//clear the graph otherwise it would become bigger and bigger!
						graph.clear();
					}


				} else {
					//ended the graph, but we have not reached the required number of triples

					//last update
					if(graph.size() >= 0) {
						cxn.begin();
						cxn.add(graph);
						cxn.commit();
						graph.clear();
					}

					//update the step, make it more thin
					//in order to reach the required number of triples
					this.step /= 2;
					//reset the offset
					localOffset = this.step;

					if(this.step <= 1) {
						//extreme case
						break;
					}
				}

			}//end while true

			//close everything
			originalConnection.close();
			reducedConnection.close();
			cxn.close();
			repo.shutDown();


		} catch (SQLException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}


	}

	/** Expands from the first triple in the provided result sets and 
	 * return a Blazegraph Graph with the expansion
	 * 
	 * @param radius maximum radius allowed
	 * */
	private Model expandSubgraph(String source, Connection cxn) throws SQLException {
		Model graph = new TreeModel();


		//execute a little bit of BFS
		Queue<RDFSimpleNode> queue = new LinkedList<RDFSimpleNode>();
		RDFSimpleNode s = new RDFSimpleNode(source, this.radius);
		queue.add(s);

		String sql_select_by_subject = 
				"select subject_, predicate_, object_ from triple_store where subject_ = ?";
		PreparedStatement ps = cxn.prepareStatement(sql_select_by_subject);
		ResultSet resSet = null;

		int iterationCounter = 0;

		while(!queue.isEmpty()) {

			RDFSimpleNode v = queue.remove();
			int vRadius = v.getRadius();

			ps.setString(1, v.getUri());
			//get all the neighbors
			resSet = ps.executeQuery();

			while(resSet.next()) {
				iterationCounter++;
				if(iterationCounter > this.maxIterationNumber) {
					//heuristic to avoid GC exception
					break;
				}

				//for each triple

				//get the data of the triple
				String subject = resSet.getString("subject_");
				String predicate = resSet.getString("predicate_");
				String object = resSet.getString("object_");

				//create the triple and add to the graph
				Statement t = BlazegraphUsefulMethods.createAStatement(subject, predicate, object);
				graph.add(t);

				if(vRadius >= 0) {
					RDFSimpleNode u = new RDFSimpleNode(object, vRadius - 1);
					queue.add(u);
				}
			}
			/*A little heuristic to avoid GC exception. 
			 * If we have too much element inside the graph and the same
			 * elements are always added, resulting in a non incrementation
			 * of the graph (which is after all only a set of triples) the
			 * compiler could lose its patience.*/
			if(iterationCounter > this.maxIterationNumber) {
				break;
			}
		}
		return graph;
	}





	/** Test main
	 * */
	public static void main(String[] args) {
		ReducedGraphCreationPhase phase = new ReducedGraphCreationPhase();
		try {
			phase.setup("properties/ReducedGraphCreationPhase.properties");
			//			phase.createAReducedGraphFromBigGraph();
			//			phase.createAReducedGraphInRDB();
			phase.createAReducedGraphFroRDBtoRDFSophisticated();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
