package it.unipd.dei.ims.rum.insight;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import it.unipd.dei.ims.rum.utilities.PathUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.UsefulConstants;

/** This class reads the information contained in 
 * different directories in pool files in 
 * order to write a csv file with the recap
 * of the performances of our algorithms.
 * <p>*/
public class RecapFileCreator {

	/** Path of the queries of the pipeline
	 * */
	private String mainQueryDirectory;

	/** The threshold for the signal to noise ration
	 * that has been used*/
	private double lambda;

	/** Path of the file where to write the information*/
	private String outputFile;

	private String pipeline;

	/** Set to true if you want to test a series of lambdas values*/
	private boolean isMultipleLambdas;
	
	/** Map with the properties*/
	private Map<String, String> map;
	
	private int numberOfQueries;
	
	private boolean isEveryone;
	
	private String[] pipelines = {"TSA+BM25", "TSA+VDP", "BLANCO", "YOSI", "SUMM"};

	public RecapFileCreator() {
		try {
			this.map = PropertiesUsefulMethods.
					getSinglePropertyFileMap("properties/RecapFileCreator.properties");

//			this.mainQueryDirectory = map.get("main.query.directory");
//			this.numberOfQueries = PathUsefulMethods.getListOfSubDirectoriesInADirectory(mainQueryDirectory).length;
			this.pipeline = map.get("pipeline");
			this.lambda = Double.parseDouble(map.get("lambda"));
//			this.outputFile = map.get("output.file");
//			this.outputFile = (new File(mainQueryDirectory)).getParent() + "/recap_" + this.pipeline + "_" + this.lambda + ".txt";
			this.isMultipleLambdas = Boolean.parseBoolean(map.get("is.multiple.lambdas"));
			this.isEveryone = Boolean.parseBoolean(map.get("everyone"));
			this.pipelines = map.get("pipelines").split(",");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void createTheRecapFileForEveryone() {
		String databaseDir = map.get("database.directory");
		for(String s : pipelines) {
			this.pipeline = s;
			if(s.equals("TSA+BM25") || s.equals("TSA+VDP") ||s.equals("TSA+VDP+BACK") || s.equals("TSA+BACK"))
				this.mainQueryDirectory = databaseDir + "/TSA/queries";			
			else if (s.equals("BLANCO"))
				this.mainQueryDirectory = databaseDir + "/SLM/queries";
			else if (s.equals("YOSI"))
				this.mainQueryDirectory = databaseDir + "/MRFKS/queries";
			else if (s.equals("SUMM"))
				this.mainQueryDirectory = databaseDir + "/SUMM/queries";
			
			System.out.println("DEBUG: " + mainQueryDirectory);
			this.outputFile = (new File(mainQueryDirectory)).getParent() + "/recap_" + this.pipeline + "_" + this.lambda + ".txt";
			this.numberOfQueries = PathUsefulMethods.getListOfSubDirectoriesInADirectory(mainQueryDirectory).length;
			this.createTheRecapFileForAllLambdas();
		}
		
	}
	
	
	
	public void createTheRecapFileForAllLambdas() {
		double[] lambdas = {0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1};
		for(double lambda : lambdas) {
			this.lambda = lambda;
			this.createTheRecapFile();
		}
	}
	
	/** Let's do this, man.
	 * <p>
	 * Creates a csv file with the recap of the most important information
	 * of the current pipeline. This method reads all of the info files 
	 * of all of the queries perfomred by the same system on the same dataset. 
	 * It creates thus a unique file containing all of these informations and the average
	 * 
	 * */
	public void createTheRecapFile() {

		//three lists to take in memory the values of precision, recall and tb-DCG
		List<Double> dcgValues = new ArrayList<Double>();
		List<Double> tbPrecValues = new ArrayList<Double>();
		List<Double> redundantPrecValues = new ArrayList<Double>();
		List<Double> setBasedPrecValues = new ArrayList<Double>();
		List<Double> recallValues = new ArrayList<Double>();
		
		List<Double> precAt1Values = new ArrayList<Double>();
		List<Double> precAt5Values = new ArrayList<Double>();
		List<Double> precAt10Values = new ArrayList<Double>();
		
		String metadata = "";
		double tbDCGMean = 0;
		double redundantPrecMean = 0;
		double tbPrecMean = 0;
		double setBasedPrecMean = 0;
		double recallMean = 0;
		double precAt1Mean = 0;
		double precAt5Mean = 0;
		double precAt10Mean = 0;
		
		
		for(int i = 1; i <= this.numberOfQueries ; ++i) {
			metadata += (i + ", ");
			//open the directory with the query
			String dirPath = this.mainQueryDirectory + "/" + i;
			File dir = new File(dirPath);
			
			double tripleBasedPrecision = 0;
			double redundantPrecision = 0;
			double recall = 0;
			double dcg = 0;
			double setBasedPrecision = 0;//it's like a precision at 1000
			double precAt1 = 0;
			double precAt5 =0;
			double precAt10 = 0;
			
			
			//reach for the information
			double[] vals = this.dealWithGenericQuery(dir, this.pipeline);
			//first value is precision
			tripleBasedPrecision = vals[0];
			recall = vals[1];
			dcg = vals[2];
			redundantPrecision = vals[3];
			setBasedPrecision = vals[4];
			
			precAt1 = vals[5];
			precAt5 = vals[6];
			precAt10 = vals[7];
			
			//add it to the list
			dcgValues.add(dcg);
			tbPrecValues.add(tripleBasedPrecision);
			recallValues.add(recall);
			redundantPrecValues.add(redundantPrecision);
			setBasedPrecValues.add(setBasedPrecision);
			
			precAt1Values.add(precAt1);
			precAt5Values.add(precAt5);
			precAt10Values.add(precAt10);
			
			//compute the mean for precision, recall and tb-DCG
			tbDCGMean += (dcg);
			tbPrecMean += tripleBasedPrecision;
			recallMean += recall;
			redundantPrecMean += redundantPrecision;
			setBasedPrecMean += setBasedPrecision;
			
			precAt1Mean += precAt1;
			precAt5Mean += precAt5;
			precAt10Mean += precAt10;
		}
		
		tbDCGMean /= this.numberOfQueries;
		tbPrecMean /= this.numberOfQueries;
		recallMean /= this.numberOfQueries;
		redundantPrecMean /= this.numberOfQueries;
		setBasedPrecMean /= this.numberOfQueries;
		
		precAt1Mean /= this.numberOfQueries;
		precAt5Mean /= this.numberOfQueries;
		precAt10Mean /= this.numberOfQueries;
		
		System.out.println("[DEBUG] " + mainQueryDirectory);
		this.outputFile = (new File(mainQueryDirectory)).getParent() + "/recap_" + this.pipeline + "_" + this.lambda + ".txt";
		Path outPath = Paths.get(this.outputFile);
		try(BufferedWriter writer = Files.newBufferedWriter(outPath, UsefulConstants.CHARSET_ENCODING)) {
			//write first line with metadata
			writer.write("#" + this.pipeline );
			writer.newLine();
			writer.write("queryNo,tbdcg,tb-precision,redundant precision,set-based precision,recall,prec@1,prec@5,prec@10");
			writer.newLine();
			//write the rest
			for(int i = 0; i < this.numberOfQueries; ++i) {
				writer.write("query " + (i+1) + "," + dcgValues.get(i)
				+ "," + tbPrecValues.get(i) + "," + redundantPrecValues.get(i) + 
				"," + setBasedPrecValues.get(i) + ","+ recallValues.get(i) +
				", " + precAt1Values.get(i) + 
				", " + precAt5Values.get(i) + 
				", " + precAt10Values.get(i));
				writer.newLine();
			}
			writer.write("means," + tbDCGMean + "," + tbPrecMean + "," 
			+ redundantPrecMean + "," + setBasedPrecMean  + "," + recallMean +
			", " + precAt1Mean +
			", " + precAt5Mean +
			", " + precAt10Mean);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**This method reads a line of a single info file
	 * and utilizes the information found to compute the
	 * meaningful measures that we use for our evaluation.
	 * 
	 * @returns an array of doubles containing precision, recall and tb-DCG*/
	private double[] dealWithGenericQuery(File dir, String method) {
		//get the path of the info file
		String infoPath = "";
		if(method.equals("YOSI"))
			infoPath = dir.getAbsolutePath() + "/evaluation/info_" + this.lambda + ".txt";
		else if(method.equals("YOSI_EXT"))
			infoPath = dir.getAbsolutePath() + "/answer/extended_evaluation/info_" + this.lambda + ".txt";
		else if(method.equals("BLANCO"))
			infoPath = dir.getAbsolutePath() + "/evaluation/info_" + this.lambda + ".txt";
		else if(method.equals("TSA+BM25"))
			infoPath = dir.getAbsolutePath() + "/BM25/evaluation/info_" + this.lambda + ".txt";
		else if(method.equals("TSA+YOSI"))
			infoPath = dir.getAbsolutePath() + "/YOSI/evaluation/info_" + this.lambda + ".txt";
		else if(method.equals("TSA+BLANCO"))
			infoPath = dir.getAbsolutePath() + "/BLANCO/evaluation/info_" + this.lambda + ".txt";
		else if(method.equals("TSA+IR-U"))
			infoPath = dir.getAbsolutePath() + "/BM25/ultimate_evaluation/info_" + this.lambda + ".txt";
		else if(method.equals("TSA+plainIR"))
			infoPath = dir.getAbsolutePath() + "/BM25/plain_evaluation/info_" + this.lambda + ".txt";
		else if(method.equals("TSA+VDP"))
			infoPath = dir.getAbsolutePath() + "/VDP/evaluation/info_" + this.lambda + ".txt";
		else if(method.equals("TSA+VDP+BACK"))
			infoPath = dir.getAbsolutePath() + "/TSA_VDP_BACK/evaluation/info_" + this.lambda + ".txt";
		else if(method.equals("TSA+BACK"))
			infoPath = dir.getAbsolutePath() + "/TSA_BACK/evaluation/info_" + this.lambda + ".txt";
		else if(method.equals("SUMM"))
			infoPath = dir.getAbsolutePath() + "/evaluation/info_" + this.lambda + ".txt";
		
		//open the file
		//System.out.println("DEBUG: info path " + infoPath + "\nand dir: " + dir.getAbsolutePath() + "\nand method " + method);
		Path inPath = Paths.get(infoPath);
		try(BufferedReader reader = Files.newBufferedReader(inPath, UsefulConstants.CHARSET_ENCODING)) {
			//take the second line, the one with the informations
			reader.readLine();
			//split the information
			String line = reader.readLine();
			//System.out.println(line);
			String[] parts = line.split(",");
			
			//XXX here we deal with the informations extrapolated from 
			//the info file. Here there must be a corrispondence
			//in what is written in the info file
			//and what you are supposing to read
			
			//take the ground truth cardinality, the denominator
			int den = Integer.parseInt(parts[0]);
			//take the number of relevant triples found, the numerator for the recall and precision
			int nrt = Integer.parseInt(parts[1]);
			//get the total number of triples that have been seen during the procedure
			//(with redundancy, that is, cunting the graphs separatedly)
			//this is the denominator for the tb-precision 
			//these are the totalTriplesSeenWithRedundancy
			double totalTriplesSeen = Integer.parseInt(parts[4]);
			double tripleBasedPrecision = (double) nrt / totalTriplesSeen;
			
			//redundant relevant triples seen
			int rrt = Integer.parseInt(parts[2]);
			double redundantPrecision = (double) (rrt / totalTriplesSeen);
			
			
			if(Double.isNaN(tripleBasedPrecision))
				tripleBasedPrecision = 0;
			
			if(Double.isNaN(redundantPrecision))
				redundantPrecision = 0;
			
			
			
			double recall = (double) nrt/den;

			double dcg = Double.parseDouble(parts[3]);
			
			//the last value is the total number of triples seen as if they where 
			//a unique set
			int totalTriplesSeenWithoutRedundancy = Integer.parseInt(parts[5].trim());
			double setBasedPrecision = (double) nrt / totalTriplesSeenWithoutRedundancy;
			
			double precAt1 = Double.parseDouble(parts[6].trim());
			double precAt5 = Double.parseDouble(parts[7].trim().trim());
			double precAt10 = Double.parseDouble(parts[8]);
			
			if(Double.isNaN(precAt1))
				precAt1 = 0;

			if(Double.isNaN(precAt5))
				precAt5 = 0;
			

			if(Double.isNaN(precAt10))
				precAt10 = 0;
			
			
			//return the values for this query
			return new double[] {tripleBasedPrecision, recall, dcg, 
					redundantPrecision, setBasedPrecision, precAt1, precAt5, precAt10};
		} catch (NoSuchFileException e) {
			System.out.println("NoSuchFileExcpetion for directory " + dir);
			System.err.println("NoSuchFileExcpetion for directory " + dir);
			return new double[] {0, 0, 0, 0, 0, 0, 0, 0};
		} catch (IOException e) {
			System.out.println("IOException for directory " + dir);
			e.printStackTrace();
		} catch (NullPointerException e) {
			System.err.println("NullPointerException for directory " + dir);
			return new double[] {0, 0, 0, 0, 0, 0, 0, 0};
		}
		return new double[] {0, 0, 0, 0, 0, 0, 0, 0};
	}
	


	

	/** Test main*/
	public static void main(String[] args) {
		RecapFileCreator execution = new RecapFileCreator();
		if(!execution.isMultipleLambdas)
			execution.createTheRecapFile();
		else {
			if(execution.isEveryone) {
				execution.createTheRecapFileForEveryone();
			} else {
				execution.createTheRecapFileForAllLambdas();
			}
		}
		
		System.out.println("all done");
	}

}
