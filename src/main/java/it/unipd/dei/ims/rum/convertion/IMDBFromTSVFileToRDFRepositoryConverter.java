package it.unipd.dei.ims.rum.convertion;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openrdf.model.Literal;
import org.openrdf.model.Model;
import org.openrdf.model.Statement;
import org.openrdf.model.Value;
import org.openrdf.model.impl.LiteralImpl;
import org.openrdf.model.impl.StatementImpl;
import org.openrdf.model.impl.TreeModel;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;

import it.unipd.dei.ims.rum.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.rum.utilities.StringUsefulMethods;
import it.unipd.dei.ims.rum.utilities.UsefulConstants;

/** This class has methods to transform a file in TSV format 
 * (Tab Separated Values) in an RDF format and insert it into a repository
 * RDF with the Blazegraph library.
 * <p>
 * This class was first needed to deal with databases like IMdb.
 * */
public class IMDBFromTSVFileToRDFRepositoryConverter {

	private static int lineCounter;

	/** Taken a TSV intended to represent an RDF graph, it saves it as RDF graph
	 * inside a TDB (Triple DataBase).
	 * <p>
	 * NB: we suppose that the fist line of the TSV file is composed by the name of the attributes.
	 * 
	 * @param inputFile the TSV file to be saved.
	 * @param outputRDFDatabase the name of the database file where to save the data.
	 * */
	@Deprecated
	public static void convertionFromTSVIntoRDFDatabaseTripleByTriple(String inputFile, String outputRDFDatabase) {
		//list of predicates given by the first line
		List<String> predicates = new ArrayList<String>();

		//input file (TSV) path
		Path inputPath = Paths.get(inputFile);

		//prepare Blazegraph database
		Repository repo = BlazegraphUsefulMethods.createRepository(outputRDFDatabase);
		RepositoryConnection cxn = null;

		//		Model model = new TreeModel();

		try {
			//get connection to database
			repo.initialize();
			cxn = repo.getConnection();

			BufferedReader reader = Files.newBufferedReader(inputPath, UsefulConstants.CHARSET_ENCODING);

			//need the first line to tell us the predicates
			String firstLine = reader.readLine();
			assert(firstLine != null);

			//take the attributes
			String[] firstLineParts = firstLine.split("\t");
			//save them in the list. We don't need the first parameter, nconst or tconst, because it simply states the fact
			//that the first value is an ID. It is not used as attribute.
			for(int i = 1; i<firstLineParts.length; ++i) {
				predicates.add(
						StringUsefulMethods.camelCaseBreakerToLowerCaseStringForURL(firstLineParts[i]));
			}

			//read the rest
			String line="";
			//keep track of how many triples have been added to the current graph
			lineCounter = 0;

			//a thread to take notice of the progression
			final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
			IMDBFromTSVFileToRDFRepositoryConverter rum = new IMDBFromTSVFileToRDFRepositoryConverter();
			scheduler.scheduleAtFixedRate(rum.new MonitorThread(), 0, 30, TimeUnit.SECONDS);
			String baseString = "https://www.imdb.com/";

			while((line = reader.readLine())!= null) {
				lineCounter++;
				//split the line and get the objects
				String[] lineParts = line.split("\t");
				//the first element is always the ID, and thus the subject
				String subject = lineParts[0].replaceAll(" ", "_");

				//for each predicate
				for(int i = 0; i < predicates.size(); ++i) {
					URIImpl pred = new URIImpl(baseString + predicates.get(i));
					//take all the object corresponding to the i-th predicate
					String[] objects = lineParts[i+1].split(",");
					for(String objct : objects) {
						objct = objct.replaceAll(" ", "_");
						//add the triple to the model
						URIImpl subj = new URIImpl(baseString + subject);
						URIImpl obj = new URIImpl(baseString + objct);
						if(objct.equals("\\N"))
							continue;
						Statement t = new StatementImpl(subj, pred, obj);
						//						model.add(t);
						//add the triple to the model
						cxn.add(t);
					}
				}//added a full line to the database
			}
			cxn.commit();
			reader.close();
			scheduler.shutdownNow();
		} catch (RepositoryException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				repo.shutDown();
				if(cxn!=null) 
					cxn.close();
			} catch (RepositoryException e) {
				e.printStackTrace();
			}
		}
	}

	/** Taken a TSV intended to represent an RDF graph, it saves it as RDF graph
	 * inside a TDB (Triple DataBase).
	 * <p>
	 * NB: we suppose that the fist line of the TSV file is composed by the name of the attributes.
	 * 
	 * @param inputFile the TSV file to be saved.
	 * @param outputRDFDatabase the name of the database file where to save the data.
	 * */
	public static void convertionFromTSVIntoRDFDatabaseByChuncks(String inputFile, String outputRDFDatabase) {
		//list of predicates given by the first line
		List<String> predicates = new ArrayList<String>();

		//input file (TSV) path
		Path inputPath = Paths.get(inputFile);

		//prepare Blazegraph database
		Repository repo = BlazegraphUsefulMethods.createRepository(outputRDFDatabase);
		RepositoryConnection cxn = null;

		Model model = new TreeModel();

		try {
			//get connection to database
			repo.initialize();
			cxn = repo.getConnection();

			BufferedReader reader = Files.newBufferedReader(inputPath, UsefulConstants.CHARSET_ENCODING);

			//need the first line to tell us the predicates
			String firstLine = reader.readLine();
			assert(firstLine != null);

			//take the attributes
			String[] firstLineParts = firstLine.split("\t");
			//save them in the list. We don't need the first parameter, nconst or tconst, because it simply states the fact
			//that the first value is an ID. It is not used as attribute.
			for(int i = 1; i<firstLineParts.length; ++i) {
				predicates.add(
						StringUsefulMethods.camelCaseBreakerToLowerCaseStringForURL(firstLineParts[i]));
			}

			//keep track of how many triples have been read from the current file
			lineCounter = 0;

			if(inputPath.getFileName().toString().equals("title.akas.tsv")) {
				//this file needs a special threatment
				dealWithTitleAkas(reader, predicates, model, cxn);
			}
			else if(inputPath.getFileName().toString().equals("title.basics.tsv")) {
				dealWithTitleBasics(reader, predicates, model, cxn);
			}
			else if(inputPath.getFileName().toString().equals("name.basics.tsv")) {
				dealWithNameBasics(reader, predicates, model, cxn);
			}
			else {
				dealWithOtherFiles(reader, predicates, model, cxn);
			}

			reader.close();
			System.out.println("First clean done");
		} catch (RepositoryException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				repo.shutDown();
				System.out.println("repo has been shut down");
				if(cxn!=null) 
					cxn.close();
				System.out.println("connection has been closed");
			} catch (RepositoryException e) {
				e.printStackTrace();
			}
		}
	}

	private static void dealWithNameBasics(BufferedReader reader, List<String> predicates, Model model, RepositoryConnection cxn) throws IOException, RepositoryException {
		//a thread to take notice of the progression
		final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
		IMDBFromTSVFileToRDFRepositoryConverter rum = new IMDBFromTSVFileToRDFRepositoryConverter();
		String line = "";

		int counter = 0;
		String baseString = "https://www.imdb.com/";
		scheduler.scheduleAtFixedRate(rum.new MonitorThread(), 0, 30, TimeUnit.SECONDS);
		while((line = reader.readLine())!= null) {
			lineCounter++;
			//split the line and get the objects
			String[] lineParts = line.split("\t");
			//the first element is always the ID, and thus the subject
			String subject = lineParts[0];
			subject = subject.replaceAll(" ", "_");
			URIImpl subj = new URIImpl(baseString + "name/" + subject);

			for(int i = 0; i<predicates.size(); ++i) {
				//for each predicate on the line
				String p_ = predicates.get(i);
				URIImpl pred = new URIImpl(baseString + predicates.get(i));
				Value o;

				//take all the object corresponding to the i-th predicate
				String[] objects = lineParts[i+1].split(",");
				for(String objct : objects) {

					if(objct.equals("\\N"))
						continue;

					if(p_.equals("primary_name")) {
						o = new LiteralImpl(objct);
					} else if(p_.equals("birth_year") ||
							p_.equals("death_year")) {
						o = new LiteralImpl(objct, new URIImpl("https://www.w3.org/TR/xmlschema11-2/#gYear"));
					} else if (p_.equals("primary_profession")) {
						o = new URIImpl(baseString + p_ + "/" + objct);
					} else if(p_.equals("known_for_titles")) {
						o = new URIImpl(baseString + "title/" + objct);
					} else {
						o = new URIImpl("/N");
					}

					Statement t = new StatementImpl(subj, pred, o);
					model.add(t);
				}


			}
			counter++;

			if(counter > 4096) {
				//flush the model inside the database
				counter = 0;
				for(Statement st:model) {
					cxn.add(st);
				}
				cxn.commit();

				//clear the model
				model.clear();
			}
		}//end of a line
		if(counter >0) {
			//flush the model inside the database
			counter = 0;
			for(Statement st:model) {
				cxn.add(st);
			}
			System.out.println("last commit...");
			cxn.commit();
			System.out.println("commit done");

			//clear the model
			model.clear();
		}
		scheduler.shutdownNow();
	}

	/**The TSV file title.akas.tsv has a particular structure, representing the various 
	 * versions of the same film translated in the different languages. Thus, it needs a slightly different procedure to 
	 * be converted in RDF triples. 
	 * @throws IOException 
	 * @throws RepositoryException 
	 * */
	private static void dealWithTitleAkas(BufferedReader reader, List<String> predicates, Model model, RepositoryConnection cxn) throws IOException, RepositoryException {
		//a thread to take notice of the progression
		final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
		IMDBFromTSVFileToRDFRepositoryConverter rum = new IMDBFromTSVFileToRDFRepositoryConverter();
		String line = "";

		int counter = 0;
		String baseString = "https://www.imdb.com/";
		scheduler.scheduleAtFixedRate(rum.new MonitorThread(), 0, 30, TimeUnit.SECONDS);
		while((line = reader.readLine())!= null) {
			lineCounter++;
			//split the line and get the objects
			String[] lineParts = line.split("\t");
			//the first element is always the ID, and thus the subject
			String subject = lineParts[0];
			subject = subject.replaceAll(" ", "_");

			//the second element is always a number describing the version of the film (different languages)
			String firstPredicate = predicates.get(0);
			String firstObject = lineParts[1];

			//create the triple
			URIImpl subj = new URIImpl(baseString + "title/" + subject);
			URIImpl pred = new URIImpl(baseString + firstPredicate);
			URIImpl obj = new URIImpl(baseString + firstPredicate + "/" + subject + "/" + firstObject);

			//add to the model
			Statement t = new StatementImpl(subj, pred, obj);
			model.add(t);

			//deal with the rest of the line
			subj = obj;
			for(int i = 1; i < predicates.size(); ++i) {
				//now the new subject becomes the old object
				String p_ = predicates.get(i);
				pred = new URIImpl(baseString + p_);

				Value o; 

				if(p_.equals("title")) {
					//take the title of the film
					String objct = lineParts[i+1].trim();
					if(objct.equals("\\N"))
						//value is NA, so we don't create a triple with an empty information as object
						continue;
					o = new LiteralImpl(objct);
					t = new StatementImpl(subj, pred, o);
					model.add(t);
				}
				else if(p_.equals("region") || p_.equals("language")) {
					String objct = lineParts[i+1].trim();
					if(objct.equals("\\N"))
						//value is NA, so we don't create a triple with an empty information as object
						continue;
					o = new URIImpl(baseString + p_ + "/" + objct);
					t = new StatementImpl(subj, pred, o);
					model.add(t);
				} else if(p_.equals("types") || p_.equals("attributes")) {
					String[] objects = lineParts[i+1].split(",");
					for(String objct : objects) { 
						if(objct.equals("\\N"))
							continue;
						o = new URIImpl(baseString + p_ + "/" +objct.replaceAll(" ", "_").trim());
						t = new StatementImpl(subj, pred, o);
						model.add(t);
					}
				} else if(p_.equals("is_original_title")) {
					String objct = lineParts[i+1];
					if(objct.equals("1")) {
						o = new LiteralImpl("true", new URIImpl("https://www.w3.org/TR/xmlschema11-2/#boolean"));
					} else {
						o = new LiteralImpl("false", new URIImpl("https://www.w3.org/TR/xmlschema11-2/#boolean"));
					}
					t = new StatementImpl(subj, pred, o);
					model.add(t);
				}
			}//end of the line
			counter++;

			if(counter > 4096) {
				//flush the model inside the database
				counter = 0;
				for(Statement st:model) {
					cxn.add(st);
				}
				cxn.commit();

				//clear the model
				model.clear();
			}
		}//the file has been read completely

		if(counter >0) {
			//flush the model inside the database
			counter = 0;
			for(Statement st:model) {
				cxn.add(st);
			}
			System.out.println("last commit...");
			cxn.commit();
			System.out.println("commit done");

			//clear the model
			model.clear();
		}
		scheduler.shutdownNow();
	}

	/** Deal with the file title.basics.tsv
	 * It has the structure:
	 * <p>
	 * tconst titleType primaryTitle originalTitle isAdult, startYear, endYear, 
	 * runtimeMinutes, genres*/
	private static void dealWithTitleBasics(BufferedReader reader, List<String> predicates, Model model, RepositoryConnection cxn) throws IOException, RepositoryException {
		//a thread to take notice of the progression
		final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
		IMDBFromTSVFileToRDFRepositoryConverter rum = new IMDBFromTSVFileToRDFRepositoryConverter();
		String line = "";

		int counter = 0;
		String baseString = "https://www.imdb.com/";
		scheduler.scheduleAtFixedRate(rum.new MonitorThread(), 0, 30, TimeUnit.SECONDS);
		while((line = reader.readLine())!= null) {
			lineCounter++;
			//split the line and get the objects
			String[] lineParts = line.split("\t");
			//the first element is always the ID, and thus the subject
			String subject = lineParts[0];
			subject = subject.replaceAll(" ", "_");
			URIImpl subj = new URIImpl(baseString + "title/" + subject);

			for(int i = 0; i<predicates.size(); ++i) {
				//for each predicate on the line
				String p_ = predicates.get(i);
				URIImpl pred = new URIImpl(baseString + predicates.get(i));
				Value o;

				if(p_.equals("primary_title") ||
						p_.equals("original_title")) {
					//title of the film. It can contain commas
					String objct = lineParts[i+1].trim();
					if(objct.equals("\\N"))
						continue;
					o = new LiteralImpl(objct);
					Statement t = new StatementImpl(subj, pred, o);
					model.add(t);
				} else if (p_.equals("is_adult")) {
					//if the film is for an adult pubblic. 
					//this information is represented with a 1 for yes
					//and with a 0 for no
					String objct = lineParts[i+1];
					if(objct.equals("\\N"))
						continue;
					else if(objct.equals("1")) {
						o = new LiteralImpl("true", new URIImpl("https://www.w3.org/TR/xmlschema11-2/#boolean"));
					} else {
						o = new LiteralImpl("false", new URIImpl("https://www.w3.org/TR/xmlschema11-2/#boolean"));
					} 
					Statement t = new StatementImpl(subj, pred, o);
					model.add(t);
				} else if(p_.equals("title_type")) {
					//type of the title (short, movie, etc.)
					String objct = lineParts[i+1];
					if(objct.equals("\\N"))
						continue;
					o = new URIImpl(baseString + p_ + "/" + objct.trim().replaceAll(" ", "_").trim());
					Statement t = new StatementImpl(subj, pred, o);
					model.add(t);
				} else if(p_.equals("start_year") ||
						p_.equals("end_year")) {
					//years of the film
					String objct = lineParts[i+1].trim();
					if(objct.equals("\\N"))
						continue;
					//gregorian Year
					o = new LiteralImpl(objct, new URIImpl("https://www.w3.org/TR/xmlschema11-2/#gYear"));
					Statement t = new StatementImpl(subj, pred, o);
					model.add(t);
				} else if(p_.equals("runtime_minutes")) {
					String objct = lineParts[i+1];
					if(objct.equals("\\N"))
						continue;
					o = new LiteralImpl(objct, new URIImpl("https://www.w3.org/TR/xmlschema11-2/#integer"));
					Statement t = new StatementImpl(subj, pred, o);
					model.add(t);
				} else if(p_.equals("genres")) {
					String[] objects = lineParts[i+1].split(",");
					for(String objct : objects) {
						if(objct.equals("\\N"))
							continue;
						o = new URIImpl(baseString + "genres/" + objct.toLowerCase());
						Statement t = new StatementImpl(subj, pred, o);
						model.add(t);
					}
				} else {
					o = new URIImpl("\\N");
					Statement t = new StatementImpl(subj, pred, o);
					model.add(t);
				}
				
			}//end of the line
			counter++;

			if(counter > 4096) {
				//flush the model inside the database
				counter = 0;
				for(Statement st:model) {
					cxn.add(st);
				}
				cxn.commit();

				//clear the model
				model.clear();
			}
		}//end of a line
		if(counter >0) {
			//flush the model inside the database
			counter = 0;
			for(Statement st:model) {
				cxn.add(st);
			}
			System.out.println("last commit...");
			cxn.commit();
			System.out.println("commit done");

			//clear the model
			model.clear();
		}
		scheduler.shutdownNow();
	}

	/** Differently from dealWithTitleAkas, all the other files have a different structure and can be treated as the same.
	 * @throws RepositoryException 
	 * */
	private static void dealWithOtherFiles(BufferedReader reader, List<String> predicates, Model model, RepositoryConnection cxn) throws IOException, RepositoryException {
		int counter = 0;
		String baseString = "https://www.imdb.com/";
		String line = "";

		final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
		IMDBFromTSVFileToRDFRepositoryConverter rum = new IMDBFromTSVFileToRDFRepositoryConverter();

		scheduler.scheduleAtFixedRate(rum.new MonitorThread(), 0, 30, TimeUnit.SECONDS);
		while((line = reader.readLine())!= null) {
			lineCounter++;
			//split the line and get the objects
			String[] lineParts = line.split("\t");
			//the first element is always the ID, and thus the subject
			String subject = lineParts[0];
			subject = subject.replaceAll(" ", "_");

			URIImpl subj = new URIImpl(baseString + "title/" + subject);
			//for each predicate
			for(int i = 0; i < predicates.size(); ++i) {
				URIImpl pred = new URIImpl(baseString + predicates.get(i));

				//take all the object corresponding to the i-th predicate
				String[] objects = lineParts[i+1].split(",");
				for(String objct : objects) {

					if(objct.equals("\\N"))
						//value is NA, so we don't create a triple with an empty information as object
						continue;

					//add the triple to the model
					boolean uriLabel = findIfUri(objct);
					Value o;
					if(uriLabel) {
						//objct is a url IMDB of the tipe tt or nm, we need to create a URI
						objct = objct.replaceAll(" ", "_");
						if(objct.startsWith("tt"))
							objct = "title/" + objct;
						else 
							objct = "name/" + objct;
						o = new URIImpl(baseString + objct);
					}
					else {
						//it is a litera
						o = new LiteralImpl(objct);
					}
					Statement t = new StatementImpl(subj, pred, o);
					model.add(t);
				}
				counter++;
			}//added a full line to the model
			if(counter > 4096) {
				//flush the model inside the database
				counter = 0;
				for(Statement st:model) {
					cxn.add(st);
				}
				cxn.commit();

				//clear the model
				model.clear();
			}
		}//end of the file
		if(counter >0) {
			//flush the model inside the database
			counter = 0;
			for(Statement st:model) {
				cxn.add(st);
			}
			System.out.println("last commit...");
			cxn.commit();
			System.out.println("commit done");

			//clear the model
			model.clear();
		}
		scheduler.shutdownNow();
	}


	/**Checks the string. If it is a string of the tipe tt(id) or
	 * nm(id) then it is a URL in the IMDB syntax, and the method 
	 * returns true.
	 * */
	private static boolean findIfUri(String objct) {
		String regex = "[nm | tt][0-9]+";
		Pattern r = Pattern.compile(regex);
		Matcher m = r.matcher(objct);
		return m.find();
	}

	private class MonitorThread extends Thread {
		public void run() {
			System.out.println("done " + lineCounter + " lines of the tsv file");
		}

	}

}
