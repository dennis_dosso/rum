package it.unipd.dei.ims.rum.convertion;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import org.apache.jena.ext.com.google.common.base.Stopwatch;
import org.openrdf.model.Value;
import org.openrdf.query.BindingSet;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;

import it.unipd.dei.ims.rum.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** Provides methods to convert a triples store in memory into a 
 * relational database table of the kind (subject, predicate, object).
 * */
public class RDFtoRDBTripleStoreConverter {

	/** Path of the database where the RDF graph is stored*/
	private String rdfDatabase;
	
	/** String to connect jdbc to the database.*/
	private String jdbcConnectingString;
	
	/**the schema this execution is referring inside the database
	 * Necessary to address the correct tables inside the database*/
	private String schema;
	
	/** SQL command to be performed when inserting the values into the database.
	 * */
	private String sqlInsert = "INSERT INTO public.triple_store(" + 
			"	subject_, predicate_, object_)" + 
			"	VALUES (?, ?, ?);";
	
	public RDFtoRDBTripleStoreConverter() {
		
	}
	
	public RDFtoRDBTripleStoreConverter (String database, String connectingString) {
		rdfDatabase = database;
		jdbcConnectingString = connectingString;
	}
	
	/** Fill the field with the information contained in the
	 * property file passed as parameter.
	 * @throws IOException 
	 * */
	public void setupFromPropertyFile(String propertyFilePath)  {
		Map<String, String> map;
		try {
			map = PropertiesUsefulMethods.getSinglePropertyFileMap(propertyFilePath);

			this.rdfDatabase = map.get("rdf.database");
			this.jdbcConnectingString = map.get("jdbc.driver");
			this.schema = map.get("schema");
			
			sqlInsert = "INSERT INTO " + this.schema + ".triple_store(" + 
					"	subject_, predicate_, object_)" + 
					"	VALUES (?, ?, ?);";
		
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/** Puts the string into the rdf store into the database.
	 * */
	public void conversion () {
		
		//get the repository to RDF
		Repository repository = BlazegraphUsefulMethods.getRepositoryFromPath(rdfDatabase);
		RepositoryConnection connection = null;
		TupleQueryResult iterator = null;
		
		//open the connection to the database
		Connection rdbConnection = null;
				
		try {
			//open RDB connection
			rdbConnection = DriverManager.getConnection(jdbcConnectingString);
			
			//prepare the insert statement
			PreparedStatement preparedInsert = rdbConnection.prepareStatement(sqlInsert);
			
			//get the rdf database
			repository.initialize();
			connection = BlazegraphUsefulMethods.getRepositoryConnection(repository);
			//get the iterator
			iterator = BlazegraphUsefulMethods.getIterator(connection);
			
			int progressiveCounter = 0;
			int totalCounter = 0;
			
			Stopwatch timer = Stopwatch.createStarted();
			
			while(iterator.hasNext()) {
				//get the triple
				BindingSet triple = iterator.next();
				//get the three amigos
				Value subject = triple.getValue("s");
				Value predicate = triple.getValue("p");
				Value object = triple.getValue("o");
				//insert them into the database
				preparedInsert.setString(1, subject.toString());
				preparedInsert.setString(2, predicate.toString());
				preparedInsert.setString(3, object.toString());
				
				preparedInsert.addBatch();
				progressiveCounter++;
				
				if( progressiveCounter>=100000 ) {
					totalCounter+=progressiveCounter;
					progressiveCounter = 0;
					
					preparedInsert.executeBatch();
					System.out.println("Inserted ... " + totalCounter + " triples in " + timer);
					
					preparedInsert.clearBatch();
					timer.stop().reset().start();
				}
			}
			if( progressiveCounter>=0 ) {
				totalCounter+=progressiveCounter;
				progressiveCounter = 0;
				
				preparedInsert.executeBatch();
				System.out.println("Last update ... " + totalCounter + " triples in " + timer);
				
				preparedInsert.clearBatch();
				timer.stop().reset().start();
			}
			
		} catch (RepositoryException e) {
			System.err.print("Error in repository init");
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if(connection!=null) {
				try {
					connection.close();
				} catch (RepositoryException e) {
					e.printStackTrace();
				}
			}
			if(rdbConnection!=null) {
				try {
					rdbConnection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	//######################
	
	/** Test main*/
	public static void main(String[] args) {
//		String rdfDatabase = "/Users/dennisdosso/Documents/RDF_DATASETS/rLinkedMDB3/dataset/dataset.jnl";
//		String jdbcConnection = "jdbc:postgresql://localhost:5432/rLinkedMDB3?user=postgres&password=Ulisse92";
		
//		RDFtoRDBTripleStoreConverter converter = new RDFtoRDBTripleStoreConverter(rdfDatabase, jdbcConnection);
		RDFtoRDBTripleStoreConverter converter = new RDFtoRDBTripleStoreConverter();
		converter.setupFromPropertyFile("properties/RDFtoRDBTripleStoreConverter.properties");
		converter.conversion();
	}
	
	//######################

	public String getRdfDatabase() {
		return rdfDatabase;
	}

	public void setRdfDatabase(String rdfDatabase) {
		this.rdfDatabase = rdfDatabase;
	}

	public String getJdbcConnectingString() {
		return jdbcConnectingString;
	}

	public void setJdbcConnectingString(String jdbcConnectingString) {
		this.jdbcConnectingString = jdbcConnectingString;
	}


	public String getSqlInsert() {
		return sqlInsert;
	}

	public void setSqlInsert(String sqlInsert) {
		this.sqlInsert = sqlInsert;
	}
}
