package it.unipd.dei.ims.rum.assessment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import org.openrdf.model.Statement;
import org.openrdf.model.impl.TreeModel;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.RDFParser;
import org.openrdf.rio.Rio;
import org.openrdf.rio.helpers.StatementCollector;

import it.unipd.dei.ims.rum.utilities.UsefulConstants;

public class RunAssessor {

	private final static String SQL_SELECT_CLUSTER_PATH = "SELECT path_string, path_id" + 
			"	FROM public.paths where path_id = ?;";
	
	private Collection<Statement> groundTruth;
	
	private Collection<Statement> groundTruthCopy;
	
	/** Size of the ground truth */
	private int groundTruthSize;
	
	private double lambda;
	private int limitRank;
	
	private String pipeline;
	
	/** the final value*/
	private double tbDiscountedCumulativeGain;
	
	
	public RunAssessor() {
		this.tbDiscountedCumulativeGain = 0;
	}
	
	

	/**Assess one run.
	 * 
	 * <p>
	 * NB: we suppose the run file to be a standard Terrier file. That is, each line is in the form:
	 * </p>
	 * <p>
	 * query_no Qxy docID rank score model_name
	 * </p>
	 * <p>
	 * Where query_no means query number. xy is the number of the query (when the run is 
	 * composed by more than one query). docID is the id of the document/graph. rank is the rank and it is
	 * meant in ascending order. Score is the score of the document provided by the model. The last
	 * element is the name of the model.
	 * </p>
	 * 
	 * @param groundTruth string with the path of the ground truth graph.
	 * @param connection a Connection object to a RDB database where the paths of the graph file 
	 * to assess are located
	 * @param runFile file .res with the ranking of the documents.
	 * @param writingFile where to write the results
	 * */
	@Deprecated
	public static void assessRun(Collection<org.openrdf.model.Statement> groundTruth, Connection connection, String runFilePath, String writingFile) {
		//open the run file
		Path inputPath = Paths.get(runFilePath);
		Path outputPath = Paths.get(writingFile);

		//read the run file
		try(BufferedReader reader = Files.newBufferedReader(inputPath, UsefulConstants.CHARSET_ENCODING)) {
			//writer
			BufferedWriter writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING);
			/*read one line at the time.*/
			String line = "";
			Collection<org.openrdf.model.Statement> graph = null;
			writer.write(GraphAssessment.attributeLine());
			writer.newLine();
			while((line = reader.readLine())!= null) {
				//for each ranked graph
				String graphPathString = parseOneTrecLineAndGetGraphPath(line, connection);
				graph = getGraphFromPathWithBlazegraph(graphPathString);

				String[] parts = line.split(" ");
				int docId = Integer.parseInt(parts[2]);
				int rank = Integer.parseInt(parts[3]);
				GraphAssessment assessment = assess(groundTruth, graph, docId, rank);

				//write down
				writer.write(assessment.toString());
				writer.newLine();
				writer.flush();
			}
			writer.close();
		} catch (IOException e) {
			System.err.println("Unable to read " + runFilePath);
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (RDFParseException e) {
			e.printStackTrace();
		} catch (RDFHandlerException e) {
			e.printStackTrace();
		}
	}


	/**Assess one run.
	 * This method uses one hash function to identify the paths of a graph
	 * <p>
	 * NB: we suppose the run file to be a standard Terrier file. That is, each line is in the form:
	 * </p>
	 * <p>
	 * query_no Qxy docID rank score model_name
	 * </p>
	 * <p>
	 * Where query_no means query number. xy is the number of the query (when the run is 
	 * composed by more than one query). docID is the id of the document/graph. rank is the rank and it is
	 * meant in ascending order. Score is the score of the document provided by the model. The last
	 * element is the name of the model.
	 * </p>
	 * Also, writes down the pool file obtained by the assessment.
	 * 
	 * @param groundTruth string with the path of the ground truth graph.
	 * @param connection a Connection object to a RDB database where the paths of the graph file 
	 * to assess are located
	 * @param runFile file .res with the ranking of the documents.
	 * @param writingFile where to write the results
	 * @param poolPath string with the path of the file where to write the pool derived by the assessment
	 * @param folding set to true if we need to use subdirectories in the answers
	 * */
	@Deprecated
	public static void assessRun(Collection<org.openrdf.model.Statement> groundTruth, 
			int step, 
			String subgraphsDirectory,
			String runFilePath, 
			String writingFile,
			String poolPath,
			boolean folding) {
		//open the run file
		Path inputPath = Paths.get(runFilePath);
		//open the file where to write
		Path outputPath = Paths.get(writingFile);
		
		Path poolOutputPath = Paths.get(poolPath);

		//read the run file
		try(BufferedReader reader = Files.newBufferedReader(inputPath, UsefulConstants.CHARSET_ENCODING) ; 
				BufferedWriter writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING) ;
				BufferedWriter poolWriter = Files.newBufferedWriter(poolOutputPath, UsefulConstants.CHARSET_ENCODING)) {
			/*read one line at the time.*/
			String line = ""; 
			Collection<org.openrdf.model.Statement> graph = null;
			writer.write(GraphAssessment.attributeLine());
			writer.newLine();

			//for each ranked graph
			while((line = reader.readLine())!= null) {
				//get the informations from the line
				String[] parts = line.split(" ");
				int docId = Integer.parseInt(parts[2]);
				int rank = Integer.parseInt(parts[3]);
				
//				if(rank%100==0) {
//					System.out.println("checked " + rank + " documents");
//				}
				//find the path to the graph
				int dirId = (int) Math.floor((double)docId / step) + 1;
				String graphPathString;
				if(folding)
					graphPathString = subgraphsDirectory + "/" + dirId + "/" + docId + ".ttl";
				else
					graphPathString = subgraphsDirectory + "/" + docId + ".ttl";
				
				//get the graph
				graph = getGraphFromPathWithBlazegraph(graphPathString);
				
				//do the assessment
				GraphAssessment assessment = assess(groundTruth, graph, docId, rank);
				
				//write down
				writer.write(assessment.toString());//informative file
				writer.newLine();
				
				poolWriter.write(assessment.poolLine());
				poolWriter.newLine();
				
				
			}
			writer.close();
			poolWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RDFParseException e) {
			e.printStackTrace();
		} catch (RDFHandlerException e) {
			e.printStackTrace();
		}

	}
	
	/**Method to evaluate a run performed by
	 * a system on a database with a certain lambda.
	 * This method creates the pool and info file that contain
	 * the information of the evaluation of a run.
	 * 
	 * @param subgraphDirectory where to find the answer subgraphs
	 * @param runFilePath path of the file .res with the ranking of the answers
	 * @param poolPath where to write the pool file
	 * @param folding if the answer graphs are contained in subdirectories*/
	public void assessRunWithTBDCG(  
			String subgraphsDirectory,
			String runFilePath, 
			String poolPath,
			int step,
			boolean folding) {
		//open the run file with the IDs of the graphs
		Path inputPath = Paths.get(runFilePath);
		
		//open the files where to write
		//the pool file
		Path poolOutputPath = Paths.get(poolPath);
		//set a file with other information
		String infoPath = (new File(poolPath)).getParent() + "/info_"+ this.lambda + ".txt";
		Path infoOutputPath = Paths.get(infoPath);

		//open all the necessary files, they will close by themselves at the end of the try
		try(BufferedReader reader = Files.newBufferedReader(inputPath, UsefulConstants.CHARSET_ENCODING) ; 
				BufferedWriter poolWriter = Files.newBufferedWriter(poolOutputPath, UsefulConstants.CHARSET_ENCODING);
				BufferedWriter infoWriter = Files.newBufferedWriter(infoOutputPath, UsefulConstants.CHARSET_ENCODING);) {
			/*read one line at the time.*/
			String line = ""; 
			//answer graph line per line
			Collection<org.openrdf.model.Statement> answerGraph = null;
			//graph that builds up while we read the new relevant triples
			Collection<org.openrdf.model.Statement> seenGraph = new TreeModel();
			
			//object representing all of the triples seen while looking at the 
			//ranking. It is the union of all of the answer graphs
			Collection<org.openrdf.model.Statement> unionGraph = new TreeModel();
			
			int rank = 0;
			
			
			
			//write the first line of the file, which is a comment
			poolWriter.write(GraphAssessment.firstLineTripleBasedPoolLine());
			poolWriter.newLine();
			//the second line, also a comment, includes the cardinality of the ground truth
			poolWriter.write("#|GT|: " + this.groundTruthSize);
			poolWriter.newLine();
			
			//number of relevant triples found in the graph
			int nrt = 0;
			//number of redundant relevant triples
			int rrt = 0;
			
			//the three denominators of the different precisions (@1, @5 and @10)
			double precAt1 = 0;
			double precAt5 = 0;
			double precAt10 = 0; 
			
			double recAt1 = 0, recAt5 = 0, recAt10 = 0;
			
			/*'pool' file. This file contains the ranking with the id of the ranked graphs,
			 * the number of relevant triples contained in them, their tb-DG (cumulative gain 
			 * which is also discounted) and their 
			 * cardinality. All these information will be used later to compute
			 * precision and recall and the complete tb-DCG of the ranking.*/

			//counter
			int c = 0;
			//the total number of triples seen
			int totalTriples = 0;
			
			//for each ranked graph
			while((line = reader.readLine())!= null && c<1000) {
				if(this.groundTruth.size()==0) {
					//already seen all the relevant triples
					break;//we are done
				}
				
				c++;
				//get t he information from the line
				String[] parts = line.split(" ");
				int docId = 0;
				int dirId = 0;
				//XXX
				//all the other pipelines
				//get the id of the answer document
				if(this.pipeline.equals("SUMM"))
					docId = Integer.parseInt(parts[2].split("_")[0]);
				else
					docId = Integer.parseInt(parts[2]);
				
				dirId = (int) Math.ceil((double)docId / step);//for everyone else
				if(this.pipeline.equals("BLANCO"))
					dirId = (int) Math.floor((double)docId / step) + 1;//for Blanco
				
				//find the path to the graph
				String graphPathString;
				if(folding)
					graphPathString = subgraphsDirectory + "/" + dirId + "/" + docId + ".ttl";
				else
					graphPathString = subgraphsDirectory + "/" + docId + ".ttl";
				
				//read the answer graph
				answerGraph = getGraphFromPathWithBlazegraph(graphPathString);
				
				if(c <= 10) {
					//I do not want for the unionGraph to become too big and occupy all of my memory
					unionGraph.addAll(answerGraph);
				}
				
				
				
				//add the number of triples seen by this graph
				//total triples is the sum of the cardinalities of all 
				//of the graphs
				totalTriples += answerGraph.size();
				
				//do the assessment
				rank++;
				GraphAssessment assessment = assessTBDCG(answerGraph, seenGraph, rank);
				assessment.setModelId(docId);
				
				//write one line in the pool file with the information about this 
				//answer graph
				poolWriter.write(assessment.tripleBasedPoolLine());
				poolWriter.newLine();
				
				//update the number of new relevant triples seen so far
				nrt += assessment.getNrt();
				//update the number of redundant relevant triples seen son far
				rrt += assessment.getRrt();
				
				if(c == 1) {
					//set the denominator for the precision at 1
					int denominatorPrecAt1 = unionGraph.size();
					int numPrecAt1 = nrt;
					precAt1 = (double) numPrecAt1 / denominatorPrecAt1;
					recAt1 = (double) numPrecAt1 / this.groundTruthSize;
				} else if(c == 5) {
					precAt5 = (double) nrt / unionGraph.size();
					recAt5 = (double) nrt / this.groundTruthSize;
				} else if(c == 10) {
					precAt10 = (double) nrt / unionGraph.size();
					recAt10 = (double) nrt / this.groundTruthSize;
				}
				
			}//end while
			
			//cover the case with rankings with less than 5 and 10 entries
			if(c < 5) {
				precAt5 = (double) nrt / unionGraph.size();
				recAt5 = (double) nrt / this.groundTruthSize;
			}
			if(c < 10) {
				precAt10 = (double) nrt / unionGraph.size();
				recAt10 = (double) nrt / this.groundTruthSize;
			}
			
			if(Double.isNaN(precAt1))
				precAt1 = 0;
			if(Double.isNaN(precAt5))
				precAt5 = 0;
			if(Double.isNaN(precAt10))
				precAt10 = 0;
			
			//now for the info file
			double recall = (double) nrt / this.groundTruthSize;
			
			//first line, which is sort of a comment. It remembers me the interpretation of the file.
			//that is: ground truth cardinality, number of relevant triples matched in total, 
			//total tb-DCG computed, total number of  triples seen (redundant and not), 
			//rts: total number of redundant relevant triples seen; lambda which we used
			infoWriter.write("|GT|, nrt matched, rrt matched, TBDCG, total triples seen with redundancy, total triples seen without redundancy (deprecated), prec@1, prec@5, prec@10, rec@1, rec@5, rec@10, total recall, lambda: " + this.lambda);
			infoWriter.newLine();
			infoWriter.write(this.groundTruthSize + "," + nrt + "," + rrt + 
					"," + this.tbDiscountedCumulativeGain + "," + totalTriples + 
					", " + unionGraph.size() + ", " + precAt1 + 
					", " + precAt5 + ", " + precAt10 + "," + recAt1 + "," + recAt5 + "," + recAt10 + "," + recall);
			
			//XXX herenyou are writing the importnt thigs. If things require too much time
			//or the graph becomes too big, delete the lines with the unionGraph.
			infoWriter.flush();
			
			//clean
			this.groundTruth.clear();
			this.groundTruthCopy.clear();
			
			//write the last piece of information
			poolWriter.write("#triple based discounted comulative gain: " + this.tbDiscountedCumulativeGain +
					"\n#total precision: " + ((double) nrt/totalTriples ) + 
					"\ntotal redundant precision: " + (double) rrt/totalTriples +
					"\ntotal list precision: " + (double)nrt/unionGraph.size());
			unionGraph.clear();
			
		} catch(NoSuchFileException e) {
			System.err.println("no run file:\n " + runFilePath + "\n");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RDFParseException e) {
			e.printStackTrace();
		} catch (RDFHandlerException e) {
			e.printStackTrace();
		} 

	}
	
	/** Returns an object representing the assessment of a graph 
	 * in the ranking given a ground truth.
	 * Originally it only computed the tb-DCG, then it was modified to compute
	 * also the number of relevant triples and also 
	 * of redundant relevant triples.
	 * 
	 * @param groundTruth the ground truth graph
	 * @param answerGraph the answer graph that we are assessing
	 * @param seenGraph the graph of the relevant triples already seen, as
	 * developed in our theory (graph S)
	 * @param lambda the signal to noise ration must be
	 * greater then this threshold to say that the graph is relevant and
	 * then compute the discounted gain.
	 * */
	public GraphAssessment assessTBDCG(
			Collection<Statement> answerGraph,
			Collection<Statement> seenGraph,
			int rank) {
		//new object assessment
		GraphAssessment assessment = new GraphAssessment();
		//set the size of this graph
		assessment.setSize(answerGraph.size());
		
		//the buffer graph is a graph keeping all the 
		//relevant triples met in this ranking
		Collection<Statement> bufferGraph = new LinkedList<Statement>();
		
		//get an iterator over the answer graph
		Iterator<Statement> iterator = answerGraph.iterator();
		
		
		//triple counter - counts the number of new relevant triples found in this answer
		int tp = 0;
		
		//redundant triple counter - counts the number of total relevant triples
		//found, even the redundant ones already seen in the ranking
		int rtp = 0;

		//look at one triple at a time inside the answer graph and see if it is relevant
		while(iterator.hasNext()) {
			//for each triple of the answer graph, check if it is relevant
			Statement t = iterator.next();
			if(this.groundTruth.contains(t)) {
				//increment the number of new relevant triples inside the answer graph
				tp++;
				bufferGraph.add(t);
			}
			
			if(this.groundTruthCopy.contains(t)) {
				rtp++;
			}
		}//end of the computation
		
		//compute the signal to noise ratio
		double signalNoiseRatio = (double) bufferGraph.size() / answerGraph.size();
		double redundantSignalNoiseRatio = (double) rtp / answerGraph.size();
		
		if(redundantSignalNoiseRatio >= lambda) {
			//set the number of redundant triples seen
			assessment.setRrt(rtp);
		} else {
			assessment.setRrt(0);
		}
		
		if(signalNoiseRatio >= lambda) {
			//the graph is relevant
			
			assessment.setTotalMatchingTriples(tp);
			
			//add the relevant triples to the graph of seen triples
			seenGraph.addAll(bufferGraph);
			//remove the seen triples to the ground truth
			groundTruth.removeAll(bufferGraph);
			
			//compute the discounted gain of this graph
			double discountedGain = (double) bufferGraph.size() / groundTruthSize;
			if(rank >= this.limitRank) {
				//we are using 2 as threshold for the discounting. Maybe should parametrize
				//in the future
				
				//compute log_b(rank) with property of logarithms (change of base, of course)
				double logDenominator = (double) Math.log(rank) / Math.log(2);
				discountedGain = (double) discountedGain / logDenominator;
			}
			
			//set the discounted gain to the graph
			assessment.setTbDG(discountedGain);
			//increment the value of the cumulative gain
			this.tbDiscountedCumulativeGain += discountedGain;
			
			//set the number of new relevant triples seen in this graph
			assessment.setNrt(bufferGraph.size());
			bufferGraph.clear();
		} else {
			//it is not relevant, the discounted gain is therefore 0
			assessment.setTbDG(0);
			assessment.setTotalMatchingTriples(0);
		}
		
		assessment.setModelRank(rank);
		
		return assessment;
	}


	private static Collection<org.openrdf.model.Statement> getGraphFromPathWithBlazegraph(String graphPath) throws RDFParseException, RDFHandlerException, IOException {
		//read the graph
		//open the input stream to the file
		File file = new File(graphPath);
		InputStream inputStream = new FileInputStream(file);
		//prepare a collector to contain the triples
		StatementCollector collector = new StatementCollector();
		RDFParser rdfParser = Rio.createParser(RDFFormat.TURTLE);
		//link the collector to the parser
		rdfParser.setRDFHandler(collector);
		//parse the file
		rdfParser.parse(inputStream, "");
		
		inputStream.close();
		
		//now get the statements composing the graph
		return collector.getStatements();
	}

	/** Given a String representing one line of a trec result file, e.g.
	 * <p>
	 * 1 Q0 1244620 4 -3.982640822322769 BM25b0.75
	 * <p>
	 * where the meaning is
	 * <p>
	 * query_number, Q0, id of the graph, rank position, score, model used
	 * <p>
	 * returns a string representing the path of the graph. 
	 * This method is used for the clustering algorithm.
	 * */
	private static String parseOneTrecLineAndGetGraphPath(String line, Connection connection) throws SQLException {
		//read one line of the trec .res file
		//eg: 1 Q0 1244620 4 -3.982640822322769 BM25b0.75
		String[] parts = line.split(" ");
		int docId = Integer.parseInt(parts[2]);

		//use the id to retrieve the graph
		PreparedStatement statement = connection.prepareStatement(SQL_SELECT_CLUSTER_PATH);
		statement.setInt(1, docId);

		ResultSet rs = statement.executeQuery();
		String graphPathString = "";
		if(rs.next()) {
			//get path of the graph
			graphPathString = rs.getString(1);
		}
		return graphPathString;
	}




	/** Returns an object representing the assessment of a graph given a ground truth.
	 * */
	public static GraphAssessment assess(Collection<Statement> groundTruth, 
			Collection<Statement> model,
			int id,
			int rank) {
		GraphAssessment assessment = new GraphAssessment();

		//get an iterator over the graph
		Iterator<Statement> iterator = model.iterator();
		//triple counter
		int tp = 0;
		int modelSize = model.size();
		int gtSize = groundTruth.size();

		while(iterator.hasNext()) {
			Statement t = iterator.next();
			if(groundTruth.contains(t)) {
				tp++;
			}
		}

		//precision
		double precision = (double) tp / modelSize;

		//recall
		double recall = (double) tp / gtSize;
		
		double fMeasure = (double) 2 * (precision * recall) / (precision + recall);

		Collection<Statement> union = new ArrayList<Statement>();
		union.addAll(groundTruth);
		union.addAll(model);
		int unionSize = union.size();
		double jaccard = (double) tp / unionSize;
		assessment.setPrecision(precision);
		assessment.setRecall(recall);
		assessment.setJaccard(jaccard);
		assessment.setfMeasure(fMeasure);
		assessment.setModelId(id);
		assessment.setModelRank(rank);
		assessment.setTotalMatchingTriples(tp);
		assessment.setGroundTruthTriples(groundTruth.size());

		//
//		decideTheRelevanceByPrecision(assessment);
		decideRelevanceByNumberOfMatchedTriples(assessment);

		return assessment;
	}

	protected static void decideTheRelevanceByPrecision(GraphAssessment ass) {
		double precision = ass.getPrecision();

		if(precision < 0.1)
			ass.setAssessment("nr");
		else
			ass.setAssessment("r");
	}
	
	protected static void decideRelevanceByNumberOfMatchedTriples(GraphAssessment ass) {
		//get the number of triples of the ground truth matched by this graph
		int tp = ass.getTotalMatchingTriples();
		
		//here 3 is a magic number
		if(tp>=3) {
			ass.setAssessment("r");
		} else {
			ass.setAssessment("nr");
		}
	}


	public Collection<Statement> getGroundTruth() {
		return groundTruth;
	}


	/** Sets the groundTruth for this assessment and also the 
	 * original size of the graph at the beginning.
	 * */
	public void setGroundTruth(Collection<Statement> groundTruth) {
		this.groundTruth = groundTruth;
		this.groundTruthSize = groundTruth.size();
		this.groundTruthCopy = new LinkedList<Statement>();
		groundTruthCopy.addAll(groundTruth);
	}


	public double getLambda() {
		return lambda;
	}


	public void setLambda(double lambda) {
		this.lambda = lambda;
	}


	public int getLimitRank() {
		return limitRank;
	}


	public void setLimitRank(int limitRank) {
		this.limitRank = limitRank;
	}


	public static String getSqlSelectClusterPath() {
		return SQL_SELECT_CLUSTER_PATH;
	}



	public String getPipeline() {
		return pipeline;
	}



	public void setPipeline(String pipeline) {
		this.pipeline = pipeline;
	}



}
