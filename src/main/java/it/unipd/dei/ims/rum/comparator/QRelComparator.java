package it.unipd.dei.ims.rum.comparator;

import java.util.Comparator;

import it.unipd.dei.ims.rum.treceval.QRelEntry;

/**Implements ordering for the elements of a Qrels in order
 * to obtain the ideal query. It performs a DESC ordering*/
public class QRelComparator implements Comparator<QRelEntry> {

	public int compare(QRelEntry t1, QRelEntry t2) {
		int val1 = t1.getRelevance();
		int val2 = t2.getRelevance();
		
		if(val1 < val2)
			return 1;
		else if (val1 > val2)
			return -1;
		return 0;
	}
}
