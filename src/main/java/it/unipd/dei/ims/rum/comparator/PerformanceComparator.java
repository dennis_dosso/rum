package it.unipd.dei.ims.rum.comparator;

import java.util.Comparator;

import it.unipd.dei.ims.datastructure.PerformanceInformation;

/** compares following the order of the query id
 * in ascending order
 * */
public class PerformanceComparator implements Comparator<PerformanceInformation> {

	
	public int compare(PerformanceInformation t1, PerformanceInformation t2) {
		int val1 = t1.getQueryId();
		int val2 = t2.getQueryId();
		
		if(val1 < val2)
			return -1;
		else if (val1 > val2)
			return 1;
		return 0;
	}
}
