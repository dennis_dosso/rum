package it.unipd.dei.ims.rum.assessment;

import org.apache.jena.rdf.model.Model;

/** Object representing the assessment of a graph.
 * */
public class GraphAssessment {
	
	/**A string describing the assessment of the graph.*/
	private String assessment;
	
	/** nrt stays for new relevant triples. Is the number of new relevant triples 
	 * found in the graph in the ordering*/
	private int nrt;
	
	/** rrt stands for redundant relevant triples. It is the 
	 * number of redundant triples found in the graph. That is,
	 * the total number of relevant triples, even the ones 
	 * already seen in the ranking*/
	private int rrt;
	
	/** the triple based Discounted Gain, as defined in our theory.
	 * */
	private double tbDG;
	
	/** ID of the query we are assessing*/
	private int queryId;
	
	/**The ground truth.*/
	private Model gt;
	
	/**The model that has been assessed*/
	private Model model;
	
	/**Id of the graph*/
	private int modelId, modelRank;
	
	private int totalMatchingTriples;
	
	/** Number of triples in the ground truth.*/
	private int groundTruthTriples;
	
	private double precision, recall, jaccard, fMeasure;
	
	/** size of the graph*/
	private int size;

	
	
	public String toString () { 
		return  modelRank + ", " + 
				modelId + ", " +
				groundTruthTriples + ", " +
				totalMatchingTriples + ", " +
				precision + ", " +
				recall + ", " +
				fMeasure + ", " +
				jaccard + ", " +
				assessment;
	}
	
	/** Returns a string which is a line of the pool*/
	public String poolLine () {
		int rel = 0;
		if(this.assessment.equals("r"))
			rel = 1;
		return "pool 0 " + modelId + " " + rel;
	}
	
	/** Writes down a string of the pool file with the 
	 * relevance judgment given by the triple based discounted gain*/
	public String tripleBasedPoolLine () {
		return "pool, " + queryId + ", " + modelId + ", " + this.nrt + ", " + this.rrt + "," + this.tbDG + ", " + this.size;
	}
	
	public static String firstLineTripleBasedPoolLine() {
		return "#name of the pool, queryId, graphId, nrt, rrt, tbCG, cardinality";
	}
	
	/** Returns a string representing the attributes this object is dealing. Useful
	 * as first line to write on top of a .csv file*/
	public static String attributeLine() {
		return "rank, ID, GT triples, total matching triples, precision, recall, F1, jaccard, relevance assessment";
	}

	
	
	
	public String getAssessment() {
		return assessment;
	}

	public void setAssessment(String assessment) {
		this.assessment = assessment;
	}

	public Model getGt() {
		return gt;
	}

	public void setGt(Model gt) {
		this.gt = gt;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

	public double getPrecision() {
		return precision;
	}

	public void setPrecision(double precision) {
		this.precision = precision;
	}

	public double getRecall() {
		return recall;
	}

	public void setRecall(double recall) {
		this.recall = recall;
	}

	public double getJaccard() {
		return jaccard;
	}

	public void setJaccard(double jaccard) {
		this.jaccard = jaccard;
	}
	

	public int getModelId() {
		return modelId;
	}

	public void setModelId(int modelId) {
		this.modelId = modelId;
	}

	public int getModelRank() {
		return modelRank;
	}

	public void setModelRank(int modelRank) {
		this.modelRank = modelRank;
	}




	public int getTotalMatchingTriples() {
		return totalMatchingTriples;
	}




	public void setTotalMatchingTriples(int totalMatchingTriples) {
		this.totalMatchingTriples = totalMatchingTriples;
	}

	public int getGroundTruthTriples() {
		return groundTruthTriples;
	}

	public void setGroundTruthTriples(int groundTruthTriples) {
		this.groundTruthTriples = groundTruthTriples;
	}

	public double getfMeasure() {
		return fMeasure;
	}

	public void setfMeasure(double fMeasure) {
		this.fMeasure = fMeasure;
	}

	public int getNrt() {
		return nrt;
	}

	public void setNrt(int nrt) {
		this.nrt = nrt;
	}

	public double getTbDG() {
		return tbDG;
	}

	public void setTbDG(double tbDG) {
		this.tbDG = tbDG;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getRrt() {
		return rrt;
	}

	public void setRrt(int rrt) {
		this.rrt = rrt;
	}
	

}
