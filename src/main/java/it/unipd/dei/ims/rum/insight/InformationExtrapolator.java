package it.unipd.dei.ims.rum.insight;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import it.unipd.dei.ims.datastructure.PerformanceInformation;
import it.unipd.dei.ims.rum.comparator.PerformanceComparator;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.UsefulConstants;

/** Class which contains useful methods to extrapolate information from 
 * log files of different kind.
 * 
 * USed for example with the log files of time.
 * Also with the recap files.
 * */
public class InformationExtrapolator {


	private Map<String, String> map;

	/** enum to describe the options we have in this execution*/
	private enum Execution {
		TIME,
		SPACE,
		TBDCG,
		RECALL,
		REDUNDANTPRECISION,
		TBPRECISION,
		SETBASEDPRECISION,
		PRECISION,
		PRECAT1,
		PRECAT5,
		PRECAT10
	}

	/** The command to decide which kind of Execution we want to perform. 
	 * To initialize through properties file
	 * */
	private Execution command;

	/** Path of the file in input*/
	private String inputFile;

	/** file in output */
	private String outputFile;

	private String addOn;

	/** Set to true if we want to read multiple files all at the same time*/
	private boolean multipleFiles;





	public InformationExtrapolator() {
		try {
			this.map = PropertiesUsefulMethods.
					getSinglePropertyFileMap("properties/InformationExtrapolator.properties");

			this.command = this.decideExecution();

			this.inputFile = map.get("input.file");
			this.multipleFiles = Boolean.parseBoolean(map.get("multiple.files"));
			if(!multipleFiles)
				this.decideOutputFile();
			this.addOn = map.get("add.on");




		} catch (IOException e) {
			System.out.println("no property file found");
			e.printStackTrace();
		}
	}

	private Execution decideExecution() {
		String com = map.get("command");
		if(com.equals("TIME"))
			return Execution.TIME;
		else if(com.equals("SPACE"))
			return Execution.SPACE;
		else if (com.equals("TBDCG"))
			return Execution.TBDCG;
		else if (com.equals("PRECISION"))
			return Execution.PRECISION;
		else if (com.equals("REDUNDANTPRECISION"))
			return Execution.REDUNDANTPRECISION;
		else if (com.equals("TBPRECISION"))
			return Execution.TBPRECISION;
		else if (com.equals("SETBASEDPRECISION"))
			return Execution.SETBASEDPRECISION;
		else if (com.equals("RECALL"))
			return Execution.RECALL;
		else if (com.equals("PRECAT1"))
			return Execution.PRECAT1;
		else if (com.equals("PRECAT5"))
			return Execution.PRECAT5;
		else if (com.equals("PRECAT10"))
			return Execution.PRECAT10;

		return null;
	}

	private void decideOutputFile() {
		File f = new File(inputFile);
		//		String filename = f.getName().split("\\.")[0];
		String filename = f.getName().replaceAll(".txt", "");
		if(this.command == Execution.TIME) {
			filename += "_time.csv";
		} else if(this.command == Execution.PRECISION) {
			filename += "_precision.csv";
		}else if(this.command == Execution.TBPRECISION) {
			filename += "_tb_precision.csv";
		}else if(this.command == Execution.REDUNDANTPRECISION) {
			filename += "_redundant_precision.csv";
		}else if(this.command == Execution.SETBASEDPRECISION) {
			filename += "_set_based_precision.csv";
		} else if(this.command == Execution.RECALL) {
			filename += "_recall.csv";
		} else if(this.command == Execution.TBDCG) {
			filename += "_tbdcg.csv";
		} else if(this.command == Execution.SPACE) {
			filename += "_space.csv";
		} else if(this.command == Execution.PRECAT1) {
			filename += "_prec@1.csv";
		} else if(this.command == Execution.PRECAT5) {
			filename += "_prec@5.csv";
		} else if(this.command == Execution.PRECAT10) {
			filename += "_prec@10.csv";
		}
		
		

		this.outputFile = map.get("output.directory") + "/" + filename;

	}

	/** Extrapolates information about time from a file and
	 * produces a csv file as output.
	 * 
	 * @param inputFile path of the input file 
	 * @param outputFile path of the outputFile
	 * */
	public List<PerformanceInformation> timeExtrapolator(String inputFile) {
		Path inPath = Paths.get(inputFile);

		//total time, to compute the average
		double totalTime = 0;
		//counter of the number of queries we have
		int counter = 0;

		List<PerformanceInformation> performanceList = new ArrayList<PerformanceInformation>();

		//NB: we deal with seconds
		try(BufferedReader reader = Files.newBufferedReader(inPath)) {
			String line = "";
			String method = "";
			String queryID = "";
			double time = 0;
			while((line = reader.readLine()) != null) {
				PerformanceInformation p = new PerformanceInformation();

				//check if the line contains the information that we want
				boolean rightLine = line.contains("TIME");
				if(rightLine) {
					counter++;
					String[] parts = line.split(" ");
					method = parts[1];
					p.setSystem(method);
					queryID = parts[2];
					time = Double.parseDouble(parts[3]);
					String unit = "";
					if(time == -1) {
						//we have finished the algorithm, so timeout
						if(parts.length >= 5) {
							try {
								time = Double.parseDouble(parts[4]);
								unit = parts[5];
							} catch(NumberFormatException e) {
								time = 1000;
								unit = "s";
							}
						} else {
							time = 1000;
							unit = "s";
						}
						p.setCompleted(0);
					} else {
						unit = parts[4];
						p.setCompleted(1);
					}

					//now, deal with the different measures
					if(unit.equals("s")) {
						//we do not need to change the unit of measure
						p.setUnitOfMeasureTime("s");
						p.setTime((int) time);
						p.setQueryId(Integer.parseInt(queryID));
					} else if(unit.equals("min")) {
						p.setUnitOfMeasureTime("s");
						//change time from minutes to seconds
						time = time * 60;
						p.setTime((int) time);
						p.setQueryId(Integer.parseInt(queryID));
					} else if(unit.equals("ms")) {
						p.setUnitOfMeasureTime("s");
						//change time from minutes to seconds
						time = (double) time / 1000;
						p.setTime((int) time);
						p.setQueryId(Integer.parseInt(queryID));
					} else {
						System.out.println("found new unit of measure " + unit);
						time = 0;
						p.setTime((int) time);
						p.setQueryId(Integer.parseInt(queryID));
					}
					//completed the execution, now insert in the list
					performanceList.add(p);
					totalTime += time;
				}

			}//end try with buffered writer
		} catch (IOException e) {
			e.printStackTrace();
		}

		//compute the average
		double averageTime = (double) totalTime / counter; 
		System.out.println("average time required by this system in this database " + averageTime);

		//order the list
		Collections.sort(performanceList, new PerformanceComparator());

		//return the list
		return performanceList;
	}





	/**Extrapolates information from a time file and print it
	 * on a csv file
	 * <p>
	 * Metadata: queryId, time (s), is completed
	 * 
	 * @param timeFile text file containing the information about the executions
	 * of a single system on different queries.
	 * */
	public void performTimeExtractionInOneFile(String timeFile, String outputFile) {
		List<PerformanceInformation> performanceList = this.timeExtrapolator(timeFile);
		//now we print the information from the list
		Path output = Paths.get(outputFile);

		try(BufferedWriter writer = Files.newBufferedWriter(output, UsefulConstants.CHARSET_ENCODING)) {
			//now we write
			for(int i = 0; i < performanceList.size(); ++i) {
				PerformanceInformation information = performanceList.get(i);
				writer.write(information.getQueryId() + ", " + information.getTime() + ", " + information.getCompleted() + "\n");
			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**Extrapolates all the informations 
	 * from a recap file
	 * */
	public List<PerformanceInformation> dataExtrapolationFromRecapFile(String inputFile) {
		List<PerformanceInformation> l = new ArrayList<PerformanceInformation>();
		double totalTBDCG = 0;
		double totalTbPrecision = 0;
		double totalRedundantPrecision = 0;
		double totalSbPrecision = 0;
		double totalRecall = 0;//like the film, yup
		
		double totalPrecAt1 = 0;
		double totalPrecAt5 = 0;
		double totalPrecAt10 = 0;
		
		int counter = 0;

		Path path = Paths.get(inputFile);
		try( BufferedReader reader = Files.newBufferedReader(path)) {
			String line = "";
			while((line = reader.readLine()) != null) {
				//take only the lines which contain the useful information
				if(line.contains("query ")) {
					counter++;
					String[] parts = line.split(",");
					//take the id of the query
					int id  = Integer.parseInt(parts[0].split(" ")[1]);
					//the first element is the query id

					//the second element is the tb-DCG
					double tbDCG = Double.parseDouble(parts[1]);
					//the third element is the tb-precision
					double tb_precision = Double.parseDouble(parts[2]);
					//the fourth is the redundant precision
					double redundant_precision = Double.parseDouble(parts[3]);
					//the fifth is the set based precision
					double sb_precision = Double.parseDouble(parts[4]);
					//the suxth is the recall
					double recall = Double.parseDouble(parts[5]);
					
					double precAt1 = Double.parseDouble(parts[6]);
					double precAt5 = Double.parseDouble(parts[7]);
					double precAt10 = Double.parseDouble(parts[8]);

					//accumulate
					totalTBDCG += tbDCG;
					totalTbPrecision += tb_precision;
					totalRedundantPrecision += redundant_precision;
					totalSbPrecision += sb_precision;
					totalRecall += recall;
					
					totalPrecAt1 += precAt1;
					totalPrecAt5 += precAt5;
					totalPrecAt10 += precAt10;

					PerformanceInformation p = new PerformanceInformation();
					p.setQueryId(id);
					p.setTbDCG(tbDCG);
					p.setPrecision(tb_precision);
					p.setTbPrecision(tb_precision);
					p.setRedunantPrecision(redundant_precision);
					p.setSbPrecision(sb_precision);
					p.setRecall(recall);
					
					p.setPrecAt1(precAt1);
					p.setPrecAt5(precAt5);
					p.setPrecAt10(precAt10);

					l.add(p);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		//read all of the lines

		//compute the averages (not really necessary)
		double averageTbDCG = (double) totalTBDCG / counter;
		double averagePrecision = (double) totalTbPrecision / counter;
		System.out.println("average tbDCG obtained by this system in this database " + averageTbDCG + 
				"\naverage precision: " + averagePrecision);

		//order the list by query ID
		Collections.sort(l, new PerformanceComparator());
		//return the list
		return l;
	}

	/**Used to extrapolate tb-DCG from recap files*/
	private void performExtractionTBDCG(String input, String out) {
		//extrapolate the information
		List<PerformanceInformation> performanceList = this.dataExtrapolationFromRecapFile(input);
		//now we print the information from the list
		Path output = Paths.get(out);

		try(BufferedWriter writer = Files.newBufferedWriter(output, UsefulConstants.CHARSET_ENCODING)) {
			//now we write
			for(int i = 0; i < performanceList.size(); ++i) {
				PerformanceInformation information = performanceList.get(i);
				writer.write(information.getQueryId() + ", " 
						+ information.getTbDCG() + "\n");
			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** A generalization of the extrapolation method, which writes a 
	 * csv file that will be used with MATLAB.
	 * 
	 * */
	private void performAllOtherExtractions(String input, String out) {
		//TODO
		
		
		
		//extrapolate the information
		List<PerformanceInformation> performanceList = this.dataExtrapolationFromRecapFile(input);
		//now we print the information from the list
		Path output = Paths.get(out);
		//open the stream
		try(BufferedWriter writer = Files.newBufferedWriter(output, UsefulConstants.CHARSET_ENCODING)) {
			//now we write
			for(int i = 0; i < performanceList.size(); ++i) {
				//decide what to write
				PerformanceInformation information = performanceList.get(i);

				//get the right information to write. These information have been gathered
				//in the dataExtrapolationFromRecapFile method above
				String toWrite = "";
				if(this.command == Execution.RECALL) {
					toWrite = ""+information.getRecall();
				} else if (this.command == Execution.REDUNDANTPRECISION) {
					toWrite = "" + information.getRedunantPrecision();
				} else if(this.command == Execution.TBPRECISION) {
					toWrite = "" + information.getTbPrecision();
				} else if(this.command == Execution.SETBASEDPRECISION) {
					toWrite = "" + information.getSbPrecision();
				} else if(this.command == Execution.PRECAT1) {
					toWrite = "" + information.getPrecAt1();
				} else if(this.command == Execution.PRECAT5) {
					toWrite = "" + information.getPrecAt5();
				} else if(this.command == Execution.PRECAT10) {
					toWrite = "" + information.getPrecAt10();
				}
				
				writer.write(information.getQueryId() + ", " 
						+ toWrite + "\n");
			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** Given a log file containing information about precision,
	 * extrapolates them and prints them in a csv file.
	 * */
	private void performExtractionPrecision(String input, String out) {
		//extrapolate the information
		List<PerformanceInformation> performanceList = this.dataExtrapolationFromRecapFile(input);
		//now we print the information from the list
		Path output = Paths.get(out);

		try(BufferedWriter writer = Files.newBufferedWriter(output, UsefulConstants.CHARSET_ENCODING)) {
			//now we write
			for(int i = 0; i < performanceList.size(); ++i) {
				PerformanceInformation information = performanceList.get(i);
				writer.write(information.getQueryId() + ", " 
						+ information.getPrecision() + "\n");
			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** Creates a list of sizes, one for each directory inside a 
	 * directory of queries
	 * 
	 * @param input path of the main directory, containing the subdirectories
	 * @param addOn element to add if you want to restrict the computation to one sub-directory*/
	public List<PerformanceInformation> computeSizesOfDirectories(String input, String addOn) {
		List<PerformanceInformation> l = new ArrayList<PerformanceInformation>();

		File mDir = new File(input);
		File[] files = mDir.listFiles();
		for(File f : files) {
			if(!f.isDirectory())
				continue;
			//get id of this query
			String id = f.getName();
			long size = this.readDirectorySizeInBytes(f.getAbsolutePath() + "/" + addOn);
			PerformanceInformation p = new PerformanceInformation();

			p.setQueryId(Integer.parseInt(id));
			p.setSize(size);

			l.add(p);
		}
		Collections.sort(l, new PerformanceComparator());
		//return the list
		return l;
	}

	/**Given the path of a directory containing other sub-directories,
	 * the method writes a csv file containing the information
	 * of the sizes of a directory.
	 * 
	 * @param inputFile path of a query directory containing all our queries
	 * @param addOn a parameter needed for special cases like BM25. When you want to 
	 * compute the size of only one sub-directory among the ones in the main 
	 * directory. If you do not have any particular preference, keep empty and it will compute
	 * the size of all the directory (with empty I mean put the empty string as value)
	 * */
	public void performExtractionOfSizes(String inputFile, String outputFile, String addOn) {
		List<PerformanceInformation> performanceList = this.computeSizesOfDirectories(inputFile, addOn);
		//now we print the information from the list
		Path output = Paths.get(outputFile);

		try(BufferedWriter writer = Files.newBufferedWriter(output, UsefulConstants.CHARSET_ENCODING)) {
			//now we write
			for(int i = 0; i < performanceList.size(); ++i) {
				PerformanceInformation information = performanceList.get(i);
				writer.write(information.getQueryId() + ", " + information.getSize() + "\n");
			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** Reads the size in bytes of a directory, with all 
	 * the content. Returns the row number in bytes.
	 * Then maybe you should use {@link fromBytesToHumanReadableSize}
	 * to have a useful string.
	 *  */
	public long readDirectorySizeInBytes(String dirPath) {
		File folder = new File(dirPath);
		//get the sie of the folder in bytes
		long size = FileUtils.sizeOfDirectory(folder);
		return size;
	}

	/** Converts a long into a corresponding human readable 
	 * size, for example in Bytes or KB of MB etc.
	 * We go up until TB.
	 * */
	public String fromBytesToHumanReadableSize(long size) {
		if(size == 0)
			return "0 MB";

		String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
		int unitIndex = (int) (Math.log10(size) / 3);
		double unitValue = 1 << (unitIndex * 10);

		String readableSize = new DecimalFormat("#,##0.#")
				.format(size / unitValue) + " "
				+ units[unitIndex];

		return readableSize;
	}


	/**Executes one computation of time, space, tbdcg or precision
	 * */
	private void execute() {
		if(this.command==Execution.TIME) {
			this.performTimeExtractionInOneFile(this.inputFile, this.outputFile);
		} else if(this.command==Execution.SPACE) {
			this.performExtractionOfSizes(inputFile, outputFile, addOn);;
		} else if(this.command==Execution.TBDCG) {
			this.performExtractionTBDCG(this.inputFile, this.outputFile);
		} else if(this.command == Execution.PRECISION) {
			this.performExtractionPrecision(this.inputFile, this.outputFile);
		} else {
			this.performAllOtherExtractions(this.inputFile, this.outputFile);
		}
	}

	/** The idea of this method is to insert inside a unique directory
	 * all the log files of a certain type.
	 * These types are TIME, TBDCG, PRECISION.
	 * In the same directory we can put many log files of the same type.
	 * This algorithm will take all these log files and, depending
	 * on the indicated type, will extrapolate the data from them and put in a corresponding 
	 * output file. The output file will be put in the output.directory.
	 * <p>
	 * NB: the SPACE case is special, since you cannot put together 
	 * directories of different methods. When dealing with SPACE; do not use 
	 * the multiple.files option and do one method at a time.
	 * */
	public void executeEverythingInsideADirectory() {
		//take al the test files inside a directory, containing the information we want to use
		String mainDirectory = this.map.get("files.directory");
		File f = new File(mainDirectory);
		//list of elements
		File[] files = f.listFiles();
		for(File file : files) {
			if(file.getName().contains(".DS_Store"))
				continue;
			//set the input file
			this.inputFile = file.getAbsolutePath();
			//re-set the output file
			this.decideOutputFile();
			this.execute();
		}
	}


	public static void main(String[] args) {
		InformationExtrapolator execution = new InformationExtrapolator();
		if(execution.isMultipleFiles()) {
			execution.executeEverythingInsideADirectory();
		} else {
			execution.execute();
		}
		System.out.println("done");
	}

	public boolean isMultipleFiles() {
		return multipleFiles;
	}

	public void setMultipleFiles(boolean multipleFiles) {
		this.multipleFiles = multipleFiles;
	}


}
