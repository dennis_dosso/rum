package test;

/** This is a test to discover how to make things wait
 * and interrupt each other*/
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


public class InterruptionTest {
	public static void main(String[] args) throws Exception {
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<String> future = executor.submit(new Task());

		try {
			//System.out.println("Started..");
			System.out.println(future.get(2, TimeUnit.SECONDS));
			System.out.println("Finished!");
		} catch (TimeoutException e) {
			future.cancel(true);
			System.out.println("The process could not terminate in time!");
		}

		executor.shutdownNow();

	}
}

class Task implements Callable<String> {
	@Override
	public String call() throws Exception {
		System.out.println("Hey there, I'm doing things here!");
		Thread.sleep(4000); // Just to demo a long running task of 4 seconds.
		System.out.println("and now I'm finished!");
		return "Ready!";
	}
}