package it.unipd.dei.ims.string;

public class CamelCaseSplitterTest {

	public static void main(String[] args) {
		String s = "deathYear";
		String[] words = s.split("(?<!^)(?=[A-Z])");
		for(String l: words) {
			l = l.toLowerCase();
			System.out.println(l);
		}
		
		System.out.println("[DEBUG]");
	}
}
