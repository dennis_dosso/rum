package it.unipd.dei.ims.query;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.jena.query.Dataset;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.tdb.TDBFactory;

/**This class tests the velocity of Jena in iterating through the triples
 * of an RDF graph.
 * */
public class NumberOfTriplesByHand implements Runnable{
	
	private static int cardinality = 0;
	private static String label = "";

	public static void main(String[] args) {
		
		
		String databaseDirectory = "/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/TDB";
		Dataset dataset = TDBFactory.createDataset(databaseDirectory);
		NumberOfTriplesByHand gnigni = new NumberOfTriplesByHand()
;		
		dataset.begin(ReadWrite.READ);
		try {
			Model model = dataset.getUnionModel();
			StmtIterator iterator = model.listStatements();
			
			final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
			scheduler.scheduleAtFixedRate(gnigni, 0, 30, TimeUnit.SECONDS);
			
			String outputFile = "/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/support/clustering/" + "labelCounterMap.csv";
			Path out = Paths.get(outputFile);

			while(iterator.hasNext()) {
				Statement t = iterator.next();
				cardinality++;
				
			}
			
			scheduler.shutdownNow();
			System.out.println("writing...");
			
		} 
		finally {
			dataset.end();
		}
		
		
		
		System.out.println("total number of triples: " + cardinality);
		
	}
	
		public void run() {
			System.out.println("Read " + cardinality + " times, last label read " + label);
	}
}
