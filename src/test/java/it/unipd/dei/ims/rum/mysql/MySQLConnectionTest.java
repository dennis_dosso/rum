package it.unipd.dei.ims.rum.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**Try to connecti to the database.
 * */
public class MySQLConnectionTest {

	public static void main(String[] args) {
		
		String connectionString = "jdbc:mysql://localhost:3306/DisGeNET?user=root&password=Ulisse92";
		try {
		    Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
		    e.printStackTrace();
		}
		
		Connection connection = null;
		
		try {
		    connection = DriverManager.getConnection(connectionString);
//		    PreparedStatement prepared = connection.prepareStatement("insert into RDF_DATABASE (name, triple_number, node_number, label_number)"
//		    		+ " values (?,?,?,?)");
		    PreparedStatement prepared = connection.prepareStatement("update RDF_DATABASE set triple_number = triple_number + 1 WHERE name='DisGeNET' ");
//		    prepared.setString(1, "DisGeNET");
//		    prepared.setInt(2, 0);
//		    prepared.setInt(3, 0);
//		    prepared.setInt(4, 0);
		    
		    PreparedStatement preparedInsert = connection.prepareStatement("insert into LABEL (LABEL_NAME, RDF_DATABASE, AVG_DEGREE, FREQUENCY) "
					+ " values (?, ?, ?, ?)");
		    preparedInsert.setString(1, "Pluto");
			preparedInsert.setString(2, "DisGeNET");
			preparedInsert.setInt(3, 0);
			preparedInsert.setInt(4, 1);
			preparedInsert.executeUpdate();
		    
//			prepared.executeUpdate();
		 
		    Statement stm = connection.createStatement();
		    ResultSet rs = stm.executeQuery("select * from LABEL");
		    while (rs.next()) {
		        System.out.println(rs.getString("LABEL_NAME") + " " + rs.getInt("AVG_DEGREE") +  rs.getInt("FREQUENCY"));
		    }
		 
		} catch (SQLException e) {
			System.out.println("mannaggia");
		    e.printStackTrace();
		} catch (Exception e) {
		    System.out.println(e.getMessage());
		} finally {
		    try {
		        if (connection != null)
		            connection.close();
		    } catch (SQLException e) {
		        // gestione errore in chiusura
		    }
		}
	}

}
