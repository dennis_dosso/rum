package it.unipd.dei.ims.rum.terrier.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.terrier.indexing.Collection;
import org.terrier.indexing.TRECCollection;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;
import org.terrier.structures.indexing.classical.BasicIndexer;
import org.terrier.utility.ApplicationSetup;
import org.terrier.utility.TagSet;

import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;

/**Class to test the indexing via a TREC collection.*/
public class TRECollectionIndexingTest {

	public static void main(String[] args) throws Exception {
		//take the useful information by the properties file
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/trec_ranking_phase.properties");

		//get the terrier home and etc directory where we have the property file and set them
		String terrierHome = map.get("terrier.home");
		String terrierEtc = map.get("terrier.etc");

		System.setProperty("terrier.home", terrierHome);
		System.setProperty("terrier.etc", terrierEtc);
		
		String aFileToIndex = "/Users/dennisdosso/Documents/developer/terrier_versions/terrier-core/share/vaswani_npl/corpus/doc-text.trec";
		String destinationIndex = "/Users/dennisdosso/Documents/developer/terrier_versions/terrier-core/var/index";
		List<String> list = new ArrayList<String>();
		list.add(aFileToIndex);
		
		BasicIndexer indexer = new BasicIndexer(destinationIndex, "data");
		Collection coll = new TRECCollection(list);
		
		indexer.index(new Collection[]{coll});

		Index index = IndexOnDisk.createIndex(destinationIndex, "data");
		System.out.println("We have indexed " + index.getCollectionStatistics().getNumberOfDocuments() + " documents");
		
	}
}
