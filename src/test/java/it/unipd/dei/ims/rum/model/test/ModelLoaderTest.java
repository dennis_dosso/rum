package it.unipd.dei.ims.rum.model.test;

import org.apache.jena.query.Dataset;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.tdb.TDBFactory;
import org.apache.jena.tdb.TDBLoader;

/**This class tests the loading of a model file in TDB.
 * */
public class ModelLoaderTest {

	public static void main(String[] args) {
		
		String supportModel = "/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/support/supportModel.ttl";
		String outputTDBDirectory = "/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/TDB/";
		
		Dataset dataset = TDBFactory.createDataset(outputTDBDirectory);
		
		dataset.begin(ReadWrite.WRITE);
		TDBLoader.loadModel(dataset.getDefaultModel(), supportModel);
		dataset.commit();
		dataset.close();
		
		System.out.println("done");
	}
}
