package it.unipd.dei.ims.rum.blazegraph.query;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

import org.openrdf.model.Statement;
import org.openrdf.query.GraphQuery;
import org.openrdf.query.GraphQueryResult;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFWriter;
import org.openrdf.rio.Rio;

import it.unipd.dei.ims.rum.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** This class queries one repository RDF.
 * */

public class RumQueryRDFBlazegraphRepository {

	/** Method to test the presence of triples inside a dataset RDF.
	 * */
	public static void queryRDFDatabaseTest(String databasePath) {
		//open the connection to the database

		Repository repo = BlazegraphUsefulMethods.getRepositoryFromPath(databasePath);
		try {
			repo.initialize();
			RepositoryConnection cxn = BlazegraphUsefulMethods.getRepositoryConnection(repo);

			//query to create a graph
			GraphQuery graphQuery = cxn.prepareGraphQuery(QueryLanguage.SPARQL, "construct where { ?s ?p ?o . } LIMIT 200");
			GraphQueryResult result = graphQuery.evaluate();
			while(result.hasNext()) {
				Statement t = result.next();
				System.out.println(t.getSubject() + " " + t.getPredicate() + " " + t.getObject());
			}

		} catch (RepositoryException e) {
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		}
	}

	/** This methods performs a CONSTRUCT query over the database
	 * 
	 * @param databasePath the path of the Blazegraph file .jnl to be interrogated
	 * @param query the SPARQL Construct query to be evaluated
	 * @param prefixString string containing all the prefixes used in the query
	 * */
	public static void queryRDFDatabase(String databasePath, String query, String prefixString, String out) {

		//open the connection to the database
		Repository repo = BlazegraphUsefulMethods.getRepositoryFromPath(databasePath);
		try {
			repo.initialize();
			RepositoryConnection cxn = BlazegraphUsefulMethods.getRepositoryConnection(repo);

			//query to create a graph
			GraphQuery graphQuery = cxn.prepareGraphQuery(QueryLanguage.SPARQL, prefixString + " " + query);
			//evaluate the query
			GraphQueryResult result = graphQuery.evaluate();

			//write the file in output
			File f = new File(out);
			OutputStream output = new FileOutputStream(f);
			RDFWriter writer = Rio.createWriter(RDFFormat.TURTLE, output);

			writer.startRDF();
			while(result.hasNext()) {
				Statement t = result.next();
				writer.handleStatement(t);
				System.out.println(t.getSubject() + " " + t.getPredicate() + " " + t.getObject());
			}
			writer.endRDF();
			output.close();

		} catch (RepositoryException e) {
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RDFHandlerException e) {
			e.printStackTrace();
		}
	}

	public static void queryRDFDatabasePrintOnlyOnSysteOut(String databasePath, String query, String prefixString, String out) {

		//open the connection to the database
		Repository repo = BlazegraphUsefulMethods.getRepositoryFromPath(databasePath);
		try {
			repo.initialize();
			RepositoryConnection cxn = BlazegraphUsefulMethods.getRepositoryConnection(repo);

			//query to create a graph
			GraphQuery graphQuery = cxn.prepareGraphQuery(QueryLanguage.SPARQL, prefixString + " " + query);
			//evaluate the query
			GraphQueryResult result = graphQuery.evaluate();


			while(result.hasNext()) {
				Statement t = result.next();
				System.out.println(t.getSubject() + " " + t.getPredicate() + " " + t.getObject());
			}

		} catch (RepositoryException e) {
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws IOException {
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/query.properties");
		String prefixString = map.get("sparql.prefix");
		String databasePath = "/Users/dennisdosso/Documents/RDF_DATASETS/LinkedMDB/RDF_database/dataset.jnl";
		String out = "/Users/dennisdosso/Desktop/test.txt";

		String query = "CONSTRUCT WHERE { ?movie movie:writer writer:13447; "
				+ "rdfs:label ?movie_label; dc:title ?movie_title . "
				+ "writer:13447 movie:writer_name ?writer_name; rdfs:label ?writer_label . }";

		query = "CONSTRUCT WHERE {?film movie:actor ?actor_id ;"
				+ "movie:director ?director_id ;"
				+ "dc:title ?film_title. "
				+ "?actor_id movie:actor_name \"Alfred Hitchcock\" ."
				+ "?director_id movie:director_name \"Alfred Hitchcock\" }";

		query = "CONSTRUCT where {?movie movie:genre ?movie_genre_id ;"
				+ "dc:title ?title . "
				+ "?movie movie:producer ?producer_id . "
				+ "?producer_id movie:producer_name ?producer_name . "
				+ "?movie_genre_id movie:film_genre_name \"Superhero\" . } ";

		query = "CONSTRUCT where {?movie movie:featured_film_location <http://data.linkedmdb.org/resource/film_location/303> ."
				+ "<http://data.linkedmdb.org/resource/film_location/303> movie:film_location_name ?name ."
				+ "?movie dc:title ?movie_titgtounle .}";

		query = "CONSTRUCT WHERE { ?film_id movie:country ?country ;"
				+ "movie:actor ?actor_id ; "
				+ "dc:title ?movie_title ;"
				+ "movie:genre ?genre_id. "
				+ "?country movie:country_name \"Spain\" ."
				+ "?genre_id movie:film_genre_name \"Biographical\" ."
				+ "?actor_id movie:actor_name \"Ed Harris\"} limit 1000";


		query = "CONSTRUCT WHERE {?film_id_1 dc:title ?film_1_title ;"
				+ "movie:prequel ?film_id_2 ; "
				+ "movie:film_subject ?film_subject ;"
				+ "movie:director ?director_id . "
				+ "?film_subject movie:film_subject_name ?subject_name ."
				+ "?director_id movie:director_name \"Steven Spielberg\" ."
				+ "?film_id_2 dc:title ?film_name_2 .} limit 500";

		query = "CONSTRUCT where {?location_id movie:film_location_name \"Venice\" ."
				+ "?movie movie:featured_film_location ?location_id ;"
				+ " dc:title \"Don Giovanni\" ;"
				+ "movie:actor ?actor_id. "
				+ "?actor_id movie:actor_name ?actor_name . "
				+ "?movie movie:genre ?genre ."
				+ "?genre movie:film_genre_name \"Opera\" ."
				+ "} LIMIT 500";

		//query di tipo 1
		query = "CONSTRUCT WHERE {?film_id movie:actor ?actor_id ;"
				+ "dc:title ?film_title ."
				+ "?actor_id movie:actor_name \"Charlie Chaplin\" . }";

		//query di tipo 2.1
		query = "CONSTRUCT WHERE { ?director_id  movie:director_name \"Wes Anderson\" ; "
				+ "foaf:made ?film_url. "
				+ "?film_url movie:actor ?actor_id; "
				+ "dc:title ?movie_title . "
				+ "?actor_id movie:actor_name \"Owen Wilson\" .}";

		//query di tipo 2.2
		query = "CONSTRUCT WHERE { ?director_id  movie:director_name \"Clint Eastwood\" ; "
				+ "foaf:made ?film_url. "
				+ "?film_url movie:genre ?genre_id; "
				+ "dc:title ?movie_title . "
				+ "?genre_id movie:film_genre_name ?genre_name .}";

		//query di tipo 3.1
		query = "CONSTRUCT WHERE {?film movie:actor ?actor_id ;"
				+ "movie:director ?director_id ;"
				+ "dc:title ?film_title. "
				+ "?actor_id movie:actor_name \"Alfred Hitchcock\" ."
				+ "?director_id movie:director_name \"Alfred Hitchcock\" }";

		//query of type 3.2
		query = "CONSTRUCT WHERE {?film movie:actor ?actor_id_1 ;"
				+ "movie:actor ?actor_id_2 ;"
				+ "dc:title \"Pretty Woman\" . "
				+ "?actor_id_1 movie:actor_name \"Julia Roberts\" ."
				+ "?actor_id_2 movie:actor_name ?actor_name_2 }";

		//query of type 4.1
		query = "CONSTRUCT where {?director_id movie:director_name ?director_name ;"
				+ "foaf:made ?film_id ."
				+ "?film_id dc:title ?film_title ;"
				+ "movie:genre ?genre_id ; "
				+ "movie:actor ?actor_id . "
				+ "?genre_id movie:film_genre_name ?genre_name ."
				+ "?actor_id movie:actor_name ?actor_name . "
				+ "}";

		//query of type 4.2
		query = "CONSTRUCT where {?director_id movie:director_name ?director_name ;"
				+ "foaf:made ?film_id ."
				+ "?film_id dc:title \"Independence Day\" ;"
				+ "movie:featured_film_location ?location_id ; "
				+ "movie:actor ?actor_id . "
				+ "?location_id movie:film_location_name ?location_name ."
				+ "?actor_id movie:actor_name ?actor_name . "
				+ "}";

		//query type 5.1
		query = "CONSTRUCT WHERE { ?film_id movie:country ?country ;"
		+ "movie:actor ?actor_id ; "
		+ "dc:title ?movie_title ;"
		+ "movie:genre ?genre_id. "
		+ "?country movie:country_name ?country_name ."
		+ "?genre_id movie:film_genre_name ?genre_name ."
		+ "?actor_id movie:actor_name \"Tom Cruise\"}";
		
		//query of type 5.2
		query = "CONSTRUCT where {?location_id movie:film_location_name \"Venice\" ."
				+ "?movie movie:featured_film_location ?location_id ;"
				+ " dc:title ?film_title ;"
				+ "movie:actor ?actor_id. "
				+ "?actor_id movie:actor_name ?actor_name . "
				+ "?movie movie:genre ?genre ."
				+ "?genre movie:film_genre_name \"Opera\" ."
				+ "}";

		RumQueryRDFBlazegraphRepository.queryRDFDatabase(databasePath, query, prefixString, out);
	}

}
