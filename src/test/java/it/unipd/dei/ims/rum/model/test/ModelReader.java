package it.unipd.dei.ims.rum.model.test;

import java.io.InputStream;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.util.FileManager;

/** This class reads a model from memory.
 * */
public class ModelReader {
	public static void main(String[] args) {
		Model model = ModelFactory.createDefaultModel();
	    String inputFileName = "/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/support/supportModel.ttl";
	    
        // use the FileManager to find the input file
        InputStream in = FileManager.get().open(inputFileName);
        if (in == null) {
            throw new IllegalArgumentException( "File: " + inputFileName + " not found");
        }
        
        // read the RDF/XML file
        model.read(in, "TTL");
        model.write(System.out, "TTL");
        
	}
}
