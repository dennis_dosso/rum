package it.unipd.dei.ims.rum.models;

import org.apache.jena.ext.com.google.common.base.Stopwatch;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.tdb.TDBFactory;
import org.apache.jena.tdb.TDBLoader;

public class ModelLoaderExecution {

	public static void main(String[] args) {
		String directory = "/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/TDB";
		System.out.println("Opening dataset...");
		
		Stopwatch timer = Stopwatch.createStarted();
		Dataset dataset = TDBFactory.createDataset(directory);
		System.out.println("Database opened in " + timer.stop());
		timer.reset().start();

		dataset.begin(ReadWrite.WRITE);
		System.out.println("starting the load");
		//path of the file to read
		String path = "/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/protein.ttl";
		TDBLoader.loadModel(dataset.getDefaultModel(), path, true);
		dataset.commit();
		dataset.close();
		
		System.out.println("everything done in "+ timer.stop());


	}
}
