package it.unipd.dei.ims.rum.relevance;

import org.apache.jena.rdf.model.Model;

/** Object representing the assessment of a graph.
 * */
public class GraphAssessment {
	
	/**A string describing the assessment of the graph.*/
	private String assessment;
	
	/**The ground truth.*/
	private Model gt;
	
	/**The model that has been assessed*/
	private Model model;
	
	/**Id of the graph*/
	private int modelId, modelRank;
	
	private int totalMatchingTriples;
	
	/** Number of triples in the ground truth.*/
	private int groundTruthTriples;
	
	private double precision, recall, jaccard, fMeasure;

	
	
	public String toString () {
		return  modelRank + ", " + 
				modelId + ", " +
				groundTruthTriples + ", " +
				totalMatchingTriples + ", " +
				precision + ", " +
				recall + ", " +
				fMeasure + "," +
				jaccard + ", " +
				assessment;
	}
	
	/** Returns a string representing the attributes this object is dealing. Useful
	 * as first line to write on top of a .csv file*/
	public static String attributeLine() {
		return "rank, ID, GT triples, total matching triples, precision, recall, F1, jaccard, relevance assessment";
	}

	
	
	
	public String getAssessment() {
		return assessment;
	}

	public void setAssessment(String assessment) {
		this.assessment = assessment;
	}

	public Model getGt() {
		return gt;
	}

	public void setGt(Model gt) {
		this.gt = gt;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

	public double getPrecision() {
		return precision;
	}

	public void setPrecision(double precision) {
		this.precision = precision;
	}

	public double getRecall() {
		return recall;
	}

	public void setRecall(double recall) {
		this.recall = recall;
	}

	public double getJaccard() {
		return jaccard;
	}

	public void setJaccard(double jaccard) {
		this.jaccard = jaccard;
	}
	

	public int getModelId() {
		return modelId;
	}

	public void setModelId(int modelId) {
		this.modelId = modelId;
	}

	public int getModelRank() {
		return modelRank;
	}

	public void setModelRank(int modelRank) {
		this.modelRank = modelRank;
	}




	public int getTotalMatchingTriples() {
		return totalMatchingTriples;
	}




	public void setTotalMatchingTriples(int totalMatchingTriples) {
		this.totalMatchingTriples = totalMatchingTriples;
	}

	public int getGroundTruthTriples() {
		return groundTruthTriples;
	}

	public void setGroundTruthTriples(int groundTruthTriples) {
		this.groundTruthTriples = groundTruthTriples;
	}

	public double getfMeasure() {
		return fMeasure;
	}

	public void setfMeasure(double fMeasure) {
		this.fMeasure = fMeasure;
	}
	

}
