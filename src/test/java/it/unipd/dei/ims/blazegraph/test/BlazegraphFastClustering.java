package it.unipd.dei.ims.blazegraph.test;

import org.openrdf.repository.RepositoryException;

import it.unipd.dei.ims.rum.clustering.test.BlazegraphStatisticsWithDatabaseTest;

public class BlazegraphFastClustering {

	public static void main(String[] args) throws RepositoryException {
		
		String databaseFile = "/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/TDB_sesame/test.jnl";
		
		try {
			System.out.println("start computing the statistics");
			BlazegraphStatisticsWithDatabaseTest stat = new BlazegraphStatisticsWithDatabaseTest();
			stat.getGraphStatistics(databaseFile, "");
			System.out.println("Statistics done");
			

		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {

		}
		System.out.println("done");
	}
}
