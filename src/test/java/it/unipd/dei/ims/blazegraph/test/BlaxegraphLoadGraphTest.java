package it.unipd.dei.ims.blazegraph.test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.jena.ext.com.google.common.base.Stopwatch;
import org.openrdf.OpenRDFException;
import org.openrdf.query.BindingSet;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;

import com.bigdata.journal.Options;
import com.bigdata.rdf.sail.BigdataSail;
import com.bigdata.rdf.sail.BigdataSailRepository;


public class BlaxegraphLoadGraphTest {

	public static void main(String[] args) throws OpenRDFException {

		final Properties props = new Properties();
		props.put(Options.BUFFER_MODE, "DiskRW"); // persistent file system located journal
//		props.put(Options.FILE, "/tmp/blazegraph/test.jnl"); // journal file location
		props.put(Options.FILE, "/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/rdf_database_test/dataset.jnl");

		final BigdataSail sail = new BigdataSail(props); // instantiate a sail
		final Repository repo = new BigdataSailRepository(sail); // create a Sesame repository

		repo.initialize();

		Stopwatch timer = Stopwatch.createStarted();
		List<String> list = new ArrayList<String>();
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/gda-batch/divided/gda_SIO_001122.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/gda-batch/divided/gda2_SIO_001121.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/gda-batch/divided/gda2_SIO_001122.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/gda-batch/gda_SIO_001119.ttl");
		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/gda_SIO_001343.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/gda-batch/gda_SIO_001124.ttl");
//		
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/gda-batch/gda_SIO_001342.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/gda-batch/gda_SIO_001343.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/gda-batch/gda_SIO_001344.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/gda-batch/gda_SIO_001345.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/gda-batch/gda_SIO_001346.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/gda-batch/gda_SIO_001347.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/gda-batch/gda_SIO_001348.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/gda-batch/gda_SIO_001349.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/disease-group.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/disease-phenotype.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/gene.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/protein.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/doClass.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/gda_score.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/hpoClass.ttl");
//		
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/pantherClass.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/pda.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/phenotype.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/pubmed.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/umlsSTY.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/variant.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/vda_score.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/vda.ttl");
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/void-disgenet.ttl");
		
//		list.add("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/raw_data/void.ttl");
		
		
		
		try {
			// open repository connection
			RepositoryConnection cxn = repo.getConnection();

			// upload data to repository
			
			try {
				cxn.begin();
				for(String s : list) {
					System.out.println("Running: " + s);
					cxn.add(new File(s), null, 
							org.openrdf.rio.RDFFormat.TURTLE);
					System.out.println("read at " + timer);
				}
				cxn.commit();
			} catch (OpenRDFException ex) {
				cxn.rollback();
				throw ex;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				// close the repository connection
				cxn.close();
			}

			// open connection
//			if (repo instanceof BigdataSailRepository) {
//				cxn = ((BigdataSailRepository) repo).getReadOnlyConnection();
//			} else {
//				cxn = repo.getConnection();
//			}
//
//			// evaluate sparql query
//			try {
//
//				final TupleQuery tupleQuery = cxn
//						.prepareTupleQuery(QueryLanguage.SPARQL,
//								"select ?s ?p ?o where { ?s ?p ?o . }");
//				TupleQueryResult result = tupleQuery.evaluate();
//				try {
//					while (result.hasNext()) {
//						BindingSet bindingSet = result.next();
//						System.err.println(bindingSet);
//					}
//				} finally {
//					result.close();
//				}
//
//			} finally {
//				// close the repository connection
//				cxn.close();
//			}

		} finally {
			repo.shutDown();
		}
		System.out.println("It's done at " + timer.stop());
	}
}
