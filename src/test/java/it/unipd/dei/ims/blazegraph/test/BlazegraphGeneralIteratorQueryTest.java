package it.unipd.dei.ims.blazegraph.test;

import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.openrdf.model.Value;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;

import com.bigdata.journal.Options;
import com.bigdata.rdf.sail.BigdataSail;
import com.bigdata.rdf.sail.BigdataSailRepository;
import com.bigdata.rdf.sail.sparql.ast.VisitorException;

/** This is a test class to use to retrieve an iterator of triples centered around a common subject.
 * 
 * What I learned: in blazegraph you need the <...> around the URI when you use it 
 * in the SPARQL queries.
 * */
public class BlazegraphGeneralIteratorQueryTest {

	public static void main(String[] args) throws RepositoryException {
		final Properties props = new Properties();
		props.put(Options.BUFFER_MODE, "DiskRW"); // persistent file system located journal
		props.put(Options.FILE, "/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/TDB_sesame_test/test.jnl"); // journal file location

		final BigdataSail sail = new BigdataSail(props); // instantiate a sail
		final Repository repo = new BigdataSailRepository(sail); // create a Sesame repository

		repo.initialize();
		
		try {
			// open repository connection
			RepositoryConnection cxn;

			// open connection
			if (repo instanceof BigdataSailRepository) {
				cxn = ((BigdataSailRepository) repo).getReadOnlyConnection();
			} else {
				cxn = repo.getConnection();
			}

			// evaluate sparql query
			try {
//				String subject = "http://identifiers.org/doid/DOID:7";
				String subject = "bagigio";
				final TupleQuery tupleQuery = cxn
						.prepareTupleQuery(QueryLanguage.SPARQL,
								"select ?p ?o where { <" + subject+"> ?p ?o . }");
				
				TupleQueryResult result = tupleQuery.evaluate();
				
				final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
				scheduler.scheduleAtFixedRate(new BlazegraphIteratortest(), 5, 30, TimeUnit.SECONDS);
				
				try {
					while (result.hasNext()) {
						//NB you have to call next, otherwise the iterator stays in the same position
						BindingSet bindingSet = result.next();
						Value pred = bindingSet.getValue("p");
						System.out.println(pred);
					}
				} finally {
					result.close();
				}
				scheduler.shutdownNow();

			} catch (MalformedQueryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (QueryEvaluationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			finally {
				// close the repository connection
				cxn.close();
			}

		} finally {
			repo.shutDown();
		}
		System.out.println("done");

	}
}
